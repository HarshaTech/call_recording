//
//  SceneDelegate.swift
//  Call Recording
//
//  Created by TechQuadra on 01/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
//        RunLoop.current.run(until: Date(timeIntervalSinceNow: 10.0))
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 5.0))
        
        let theme = Application.getBoolValueFromUserDefaults(forKey: C.Stored._darkModeEnabled)
        if theme {
            C.theme = AppTheme(_theme: Theme.dark)
            NotificationCenter.default.post(name: .darkModeEnabled, object: nil)
        } else {
            C.theme = AppTheme(_theme: Theme.light)
            NotificationCenter.default.post(name: .darkModeDisabled, object: nil)
        }


        guard let winScene = (scene as? UIWindowScene) else { return }
        
        let userlogin = Application.getBoolValueFromUserDefaults(forKey: Application.Stored.isLogin)
        if userlogin {
            let vc = ViewController()
            let navigationController = UINavigationController(rootViewController: vc)
            let win = UIWindow(windowScene: winScene)
            win.rootViewController = navigationController
            win.makeKeyAndVisible()
            window = win
        } else {
            let vc = ViewController()
            let navigationController = UINavigationController(rootViewController: vc)
            let win = UIWindow(windowScene: winScene)
            win.rootViewController = navigationController
            win.makeKeyAndVisible()
            window = win
        }

        

    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

