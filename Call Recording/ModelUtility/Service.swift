//
//  Service.swift
//  Troober
//
//  Created by Akshay Somkuwar on 26/04/19.
//  Copyright © 2018 vyclean.com. All rights reserved.
//


import Foundation
import Alamofire
import NVActivityIndicatorView

struct CError {
    var status: Bool //= false
    var message: String //= ""
    init() {
        status = false
        message = ""
    }
    init(_message: String) {
        status = true
        message = _message
    }
}

class Service: NSObject {
    static let shared = Service()
    
    var header: HTTPHeaders = [
        "content-type": "application/json"
    ]
    
    func checkToken(accessToken: String?){
        if let token = accessToken {
            header["Authorization"] = "Bearer " + token
        }
    }
    
    func getResponse(url: String, parameters: [String: Any]?, _ completion: @escaping (Any?, Any?) -> Void, error:  @escaping (CError?) -> Void) {
        App.activityIndicator.startAnimating()
        AF.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print(response.value)
            App.activityIndicator.stopAnimating()
            switch response.result {
            case .success:
                App.activityIndicator.stopAnimating()
                if response.data != nil {
                    completion(response.value, response.data)
                } else {
                    error(CError(_message: "Invalid Response"))
                }
            case .failure(let _error):
                App.activityIndicator.stopAnimating()
                error(CError(_message: _error.localizedDescription))
            }
        }
    }
    
    func postRequest(url: String, isJsonEncoding: Bool, parameters: [String: Any]?, _header: HTTPHeaders?, _ completion: @escaping (Any?, Any?) -> Void, error:  @escaping (CError?) -> Void) {
    
        print(url)
        print("Call started")
        
//        let manager = Alamofire.SessionManager.default
//        manager.session.configuration.timeoutIntervalForRequest = 500

        AF.request(url, method: HTTPMethod.post, parameters: parameters, encoding: isJsonEncoding ? JSONEncoding.default : URLEncoding(destination: .methodDependent), headers: _header).responseJSON { (response) in
            print("Call ended")
            print(response)
            print(response.value)
            App.activityIndicator.stopAnimating()
            switch response.result {
            case .success:
                if response.data != nil {
                    completion(response.value, response.data)
                } else {
                    error(CError(_message: "Something went wrong, please try after sometime"))
                }
            case .failure(let _error):
                //error(CError(_message: _error.localizedDescription))
                print(_error.localizedDescription)
                error(CError(_message: _error.localizedDescription))
            }
        }
    }
    
    
    func request(webName: String,whoswebcall:Bool, APIType: HTTPMethod, params: [String:Any], completionBlock:@escaping(Any) -> (), error:@escaping(Any) -> ()) {
        //let URL = ""//C.baseUrl + webName
        
        var encoding = whoswebcall
        
        if(APIType == .post || APIType == .delete || APIType == .patch){
            header = [:]
            encoding = true
        }
        
        checkToken(accessToken: UserDefaults.standard.value(forKey: "token") as? String)
        AF.request(webName.trimmingCharacters(in: .whitespacesAndNewlines), method: APIType, parameters: params, encoding: encoding ? JSONEncoding.default : URLEncoding.httpBody, headers: header).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            if statusCode == 401 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let JSON):
                    error(JSON)
                }
            } else if statusCode == 200 {
                switch response.result {
                case .success(let JSON):
                    if(whoswebcall){
                        completionBlock(JSON)
                    } else {
                        print(JSON)
                        completionBlock(response.data!)
                    }
                case .failure(let err):
                    completionBlock(err)
                }
            } else if statusCode == 400 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let err):
                    error(err)
                }
            } else if statusCode == 422 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let JSON):
                    error(JSON)
                }
            }
        }
    }
}

class App {

static let activityIndicator: NVActivityIndicatorView = {
    var centerX: CGFloat?
    var centerY: CGFloat?
    
    centerX = UIScreen.main.bounds.width / 2
    centerY = UIScreen.main.bounds.height / 2
    
    var AV = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: NVActivityIndicatorType.ballScaleRippleMultiple, color: UIColor.red, padding: 5)
    AV.layer.cornerRadius = 25
    AV.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    AV.center = CGPoint(x: centerX! , y: centerY!)
    //AV.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    return AV
    
    }()
}
