
//
//  Device.swift
//  Klictic Vendor
//
//  Created by BBC on 23/07/18.
//  Copyright © 2018 Vyclean Solutions. All rights reserved.
//

import Foundation
import UIKit


import Foundation
import UIKit


struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    
    static func navigationBarHeight() -> CGFloat {
        var size: CGFloat = 0
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                size = 70
            case 1334:
                print("iPhone 6/6S/7/8")
                size = 70
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                size = 70
            case 2436:
                print("iPhone X, Xs")
                size = 90
            case 2688:
                print("iPhone Xs Max")
                size = 90
            case 1792:
                print("iPhone Xr")
                size = 90
            default:
                print("unknown")
                size = 70
            }
        } else {
            size = 70
        }
        return size
    }
    
    
}
