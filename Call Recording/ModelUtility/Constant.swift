//
//  Constant.swift
//  REAL ESTATE
//
//  Created by TechQuadra on 03/10/19.
//  Copyright © 2019 Vyclean Solutions. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CoreMotion


class Application {
    
    
    struct API {
        static let termsAndCondUrl = "https://adstocashsl.com/tof"
        static let privacyUrl = "https://adstocashsl.com/privacy"
    }
    
//        static let appBlue = UIColor.init(rgb: 0x2699FB)
        struct Colors {
            static let appBlue = UIColor.init(rgb: 0x31BEFF)
            static let appGreen = UIColor.init(rgb: 0x198c19)//0x31BEFF
            static let appViolet = UIColor.init(rgb: 0xB14EF9)
            static let appLightGreen = UIColor.init(rgb: 0x4ecaaa)
            static let appDarkGreen = UIColor.init(rgb: 0x00c853)
            static let appYellow = UIColor.init(rgb: 0xffa500)
            static let primaryColor = UIColor.init(rgb: 0x85c86f)
            static let drawerColor = UIColor.init(rgb: 0xFF02BB82)
            static let primaryYellowColor = UIColor.init(rgb: 0xF9CA29)
            static let pathColor = UIColor.init(rgb: 0xFF0292F0)
            
            static let primaryLightColor = UIColor.init(rgb: 0xED0714)
            static let primaryDarkColor = UIColor.init(rgb: 0x121212)
            static let primaryLightLightColor = UIColor.init(rgb: 0xF35643)
            static let primaryDarkLightColor = UIColor.init(rgb: 0x121212)
            static let navBarDarkColor = UIColor.init(rgb: 0x121212)
            
            static let mattDark = UIColor.init(rgb: 0x1D201F)
            static let mattLight = UIColor.init(rgb: 0x282E2C)
            static let darkThemeBlueColor = UIColor.init(rgb: 0xd531beff)
            
            static let matchInfoLightThemeDarkColor = UIColor.init(rgb: 0xE9E9E9)
            
            static var appPinkColor = UIColor.init(red: 241/255, green: 107/255, blue: 140/255, alpha: 1)
            static var appBlueColor = UIColor.init(red: 66/255, green: 158/255, blue: 226/255, alpha: 1)
            static var appRedColor = UIColor.init(red: 209/255, green: 31/255, blue: 47/255, alpha: 1)
            
            static let yesBGColor = UIColor.init(rgb: 0x4a9dd6)
            static let noBGColor = UIColor.init(rgb: 0xf16b8c)
            
            static let darkThemeLightDarkColor = UIColor.init(rgb: 0x4d5359)
            static let lightThemeLightRedColor = UIColor.init(rgb: 0xCD2938)
            static let lightOrangeColor = UIColor.init(rgb: 0xF14E47)
            static let newLightThemeRedColor = UIColor.init(rgb: 0xed4549)
            
            static let pollsYellowColor = UIColor.init(rgb: 0xfaca2c)
            static let pollsOrangeColor = UIColor.init(rgb: 0xe75e4e)
            static let pollsGreenColor = UIColor.init(rgb: 0x4cb050)

//        static let appBlue = UIColor.init(rgb: 0x753E9D)
        static let buttonColor = UIColor.init(rgb: 0x753E9D)
    }
    
    class func setValueToUserDefaults(_ value: Bool, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    class func getValueFromUserDefaults(forKey: String) -> Bool {
        var _value = false
        let defaults = UserDefaults.standard
        if let value = defaults.object(forKey: forKey) as? Bool {
            _value = value
        } else {
            _value = false
        }
        return _value
    }
    
    class func setValueAsStringToUserDefaults(_ value: String, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    class func getValueAsStringFromUserDefaults(forKey: String) -> String {
        var _value = ""
        let defaults = UserDefaults.standard
        if let value = defaults.object(forKey: forKey) as? String {
            _value = value
        } else {
            _value = ""
        }
        return _value
    }
    
    class func setBoolValueToUserDefaults(_ value: Bool, forKey: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: forKey)
        defaults.synchronize()
    }
    
    class func getBoolValueFromUserDefaults(forKey: String) -> Bool {
        var _value = false
        let defaults = UserDefaults.standard
        if let value = defaults.object(forKey: forKey) as? Bool {
            _value = value
        } else {
            _value = false
        }
        return _value
    }

    
    class func attributedTextWith(text1: String, text2: String, font1: UIFont, font2: UIFont, color1: UIColor, color2: UIColor, text3: String? = nil, font3: UIFont? = nil, color3: UIColor? = nil) -> NSMutableAttributedString {
        var matchAttributedStr = NSMutableAttributedString()
        let font1 = font1//UIFont(name: "HelveticaNeue-Bold", size: 20)
        let font2 = font2//(name: "HelveticaNeue-Bold", size: 40)"₹"
        let _text1 = NSMutableAttributedString(string: text1, attributes: [
            .font: font1,
            .foregroundColor: color1
            ])
        let _text2 = NSMutableAttributedString(string: text2, attributes: [
            .font: font2,
            .foregroundColor: color2
            ])
        let _text3 = NSMutableAttributedString(string: text3 != nil ? text3 ?? "" : "", attributes: [
            .font: font3 != nil ? font3 ?? font2 : font2,
            .foregroundColor: color3
            ])
        matchAttributedStr.append(_text1)
        matchAttributedStr.append(_text2)
        matchAttributedStr.append(_text3)
        return matchAttributedStr
    }

    class func isEmail(email: String) -> Bool {
        let emailTest = NSPredicate(format:"SELF MATCHES %@", Constant.Validations.emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    struct Constant {
        struct Colors {
            static let appBlue = UIColor.init(rgb: 0x31BEFF)
        }
        
        struct Validations {
            static let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            static let passwordRegEx = "^(?=.*?[0-9])(?=.*?[a-zA-Z]).{3,}$"
        }
    }
    
    class func showAlertWithActions(title: String?, subTitle: String?, message: String?, actionTitle: String, completion: @escaping (UIAlertController) -> Void, firstAction: @escaping (UIAlertAction?) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default) { (alert) in
            firstAction(alert)
        }//UIAlertAction.init(title: actionTitle, style: .default, handler: nil)
        alert.addAction(action)
        completion(alert)
    }
    
    class func showAlertWithActionsTwo(title: String?, subTitle: String?, message: String?, actionTitle: String ,actionTitle2: String, completion: @escaping (UIAlertController) -> Void, firstAction: @escaping (UIAlertAction?) -> Void , secondAction: @escaping (UIAlertAction?) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default) { (alert) in
            firstAction(alert)
        }
        let action2 = UIAlertAction(title: actionTitle2, style: .default) { (alert) in
            secondAction(alert)
        }
        alert.addAction(action)
        alert.addAction(action2)
        completion(alert)
    }


    class func showAlert(title: String?, actionTitle: String, vc: UIViewController) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let action = UIAlertAction.init(title: actionTitle, style: .default, handler: nil)
        alert.addAction(action)
        vc.present(alert, animated: true, completion: nil)
    }

    class func showAlert(title: String?, subTitle: String?, message: String?, actionTitle: String, completion: @escaping (UIAlertController) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: actionTitle, style: .default, handler: nil)
        alert.addAction(action)
        completion(alert)
    }
    
    class func setValueToUserDefaults(_ value: String, forKey: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: forKey)
        defaults.synchronize()
    }
    
    class func getValueFromUserDefaults(forKey: String) -> String {
        var _value = ""
        let defaults = UserDefaults.standard
        if let value = defaults.object(forKey: forKey) as? String {
            _value = value
        } else {
            _value = ""
        }
        return _value
    }
//
//    class func getBoolValueFromUserDefaults(forKey: String) -> Bool {
//        var _value = false
//        let defaults = UserDefaults.standard
//        if let value = defaults.object(forKey: forKey) as? Bool {
//            _value = value
//        } else {
//            _value = false
//        }
//        return _value
//    }
    
    struct Stored {
        static let user_id = "user_id"
        static let isLogin = "isLogin"
        static var accountNo = ""
        static var UserType_id = ""
        static var signUpParam = [String: Any]()
        static var selectedCustomerId = ""
        static var selectedAccNo = ""
        static var isCustomer = "isCustomer"
        static var _customer = false
        static var _moveToDash = false
        static var isBackToAudio = true
        static var selectCode = "_countryCode"
        static var selectCountry = "_country"
        
    }
    
    class func showActionSheet(completion: @escaping (UIAlertController) -> Void, firstAction: @escaping (UIAlertAction?) -> Void , secondAction: @escaping (UIAlertAction?) -> Void, thirdAction: @escaping (UIAlertAction?) -> Void) {
        let alert:UIAlertController = UIAlertController(title: "Choose Image",
                                                        message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (alert) in
            firstAction(alert)
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (alert) in
            secondAction(alert)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { (alert) in
            thirdAction(alert)
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        completion(alert)
//        self.present(alert, animated: true, completion: nil)
    }
    
    class func OpenCameraAction(completion: @escaping (UIImagePickerController) -> Void) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            //            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
//            myPickerController.allowsEditing = true
            //            self.present(myPickerController, animated: true, completion: nil)
            completion(myPickerController)
        }

    }
    
    class func OpenGallery(completion: @escaping (UIImagePickerController) -> Void) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.sourceType = .photoLibrary
//            myPickerController.allowsEditing = true
            completion(myPickerController)
        }
    }
}

class CustomNavigationBar: UIView {

    //var delegate: UserNavigationBarDelegate?
    var customNavigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = Appl
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()
    
    var backButton: CustomButton = {//UIButton = {
        let button = CustomButton()//UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        //        button.backgroundColor = UIColor.green
        let image = #imageLiteral(resourceName: "back dark").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        button.setImage(image, for: UIControl.State.normal)
        button.tintColor = C.theme.textColor
        button.imageView?.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        return button
    }()

    var titleL: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.textAlignment = .center
        label.textColor = C.theme.textColor
//        label.backgroundColor = .red
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.1
        label.font = UIFont(name: "Montserrat-Medium", size: 22)//Constant.Font.appFont(size: 14)//UIFont.fontAwesome(ofSize: 13, style: FontAwesomeStyle.solid)
        label.text = ""// + String.fontAwesomeIcon(name: FontAwesome.bolt) + " Ninja"
        return label
    }()
    
    
    var isBackButton = false {
        didSet{
            self.layoutSubviews()
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(_isBack: Bool) {
        self.init()
        self.isBackButton = _isBack
        setupNavigationBar()
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    @objc func backButtonTest() {
        print("Back Button test")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupNavigationBar() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)


//        self.customNavigationBarView.backgroundColor = UIColor.white
        self.addSubview(customNavigationBarView)
        self.addSubview(backButton)
        self.addSubview(titleL)
        
        customNavigationBarView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self).offset(0)
            make.height.equalTo(Device.navigationBarHeight())
        }
        
        if isBackButton {
            backButton.isHidden = false
            backButton.snp.makeConstraints { (make) in
                make.left.equalTo(customNavigationBarView.snp.left).offset(10)
                make.bottom.equalTo(customNavigationBarView.snp.bottom).offset(-15)
                make.width.height.equalTo(22)
            }

        } else {
            backButton.isHidden = true
        }

        titleL.snp.makeConstraints { (make) in
            make.height.equalTo(25)
            make.bottom.equalTo(customNavigationBarView.snp.bottom).offset(-10)
            make.left.equalTo(self).offset(70)
            make.right.equalTo(self).offset(-70)
        }
    }
    
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.customNavigationBarView.backgroundColor = C.theme.navColor
        self.titleL.textColor = C.theme.textColor
        
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.customNavigationBarView.backgroundColor = .white
        self.titleL.textColor = C.theme.textColor
    }


    
    internal func followUser(sender: UIButton){
        print("follow")
    }
    
    //END VC
}

final class CustomButton: UIButton {
    static  let checkbox = CustomButton()
    typealias isPressed = (CustomButton) -> ()
    var pressed :isPressed? {
        didSet {
            if pressed != nil {
                print("backButton Is Pressed  NOT Nil")
                addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
            } else {
                print("backButton Is Pressed Nil")
                removeTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CustomButton.buttonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        
        if let handler = pressed {
            print("class back button")
            handler(self)
        } else {
            print("pressed is Nil")
        }
    }
    
}

class ButtonBarV: UIView {
    var customNavigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()
    
    var packL: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.black
        l.numberOfLines = 0
        l.font = UIFont(name: "", size: 16)
        return l
    }()
    
    var checkBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "untick").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        btn.tintColor = UIColor.init(rgb: 0x057D08)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    var descL: UILabel = {
        let l = UILabel()
        l.textAlignment = .right
        l.numberOfLines = 0
        l.font = UIFont(name: "", size: 13)
        l.textColor = UIColor.black
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNavigationBar()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setNavigationBar()  {
        self.addSubview(customNavigationBarView)
        customNavigationBarView.addSubview(packL)
        customNavigationBarView.addSubview(checkBtn)
        customNavigationBarView.addSubview(descL)
        
        customNavigationBarView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self).offset(0)
            make.height.equalTo(50)
        }

        checkBtn.snp.makeConstraints { (make) in
            make.right.equalTo(customNavigationBarView).offset(-15)
            make.centerY.equalTo(customNavigationBarView)
            make.height.width.equalTo(25)
        }
        
        packL.snp.makeConstraints { (make) in
            make.left.equalTo(customNavigationBarView).offset(15)
            make.centerY.equalTo(customNavigationBarView)
            
        }
        
        descL.snp.makeConstraints { (make) in
            make.right.equalTo(checkBtn.snp.left).offset(-10)
            make.centerY.equalTo(customNavigationBarView)
            make.width.equalTo(200)
        }
    }
    
}

class SettingViews: UIView {
    var customNavigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = C.theme.navColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()
    
    var iconImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "btn")
        image.clipsToBounds = true
        return image
    }()
    
    var labelText: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "Montserrat-Regular", size: 14)
        l.textColor = C.theme.textColor
        return l
    }()
    
    var rightArrowImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "icons8-forward-24").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        image.tintColor = C.theme.textColor
        image.clipsToBounds = true
        return image
    }()
    
    var rightL: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "Montserrat-Regular", size: 10)
        l.textColor = C.theme.textColor
        l.textAlignment = .right
        return l
    }()
    
    var switchBtn = SwitchButton()
    
    var isArrowButton = false {
        didSet{
            self.layoutSubviews()
            self.layoutIfNeeded()
        }
    }
    
    var isSwitchButton = false {
        didSet{
            self.layoutSubviews()
            self.layoutIfNeeded()
        }
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(_isArrow: Bool, _isSwitch: Bool, image: UIImage, title: String) {
        self.init()
        self.isArrowButton = _isArrow
        self.isSwitchButton = _isSwitch
        iconImage.image = image
        labelText.text = title
        setupNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)

    }
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
//        self.customNavigationBarView.backgroundColor = C.theme.backgroundColor
        self.customNavigationBarView.backgroundColor = Application.Colors.mattLight
        self.labelText.textColor = C.theme.textColor
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
//        self.view.backgroundColor = .white
        self.customNavigationBarView.backgroundColor = C.theme.navColor
        self.labelText.textColor = C.theme.textColor

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkModeEnabled, object: nil)
        NotificationCenter.default.removeObserver(self, name: .darkModeDisabled, object: nil)
    }



    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    @objc func backButtonTest(){
        print("Back Button test")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupNavigationBar() {
        self.addSubview(customNavigationBarView)
        self.addSubview(iconImage)
        self.addSubview(labelText)
        self.addSubview(rightArrowImage)
        self.addSubview(rightL)
        self.addSubview(switchBtn)
        
        if C.theme.theme == Theme.light {
            NotificationCenter.default.post(name: .darkModeDisabled, object: nil)
        } else {
            NotificationCenter.default.post(name: .darkModeEnabled, object: nil)
        }

        
        if isSwitchButton != true {
            switchBtn.isHidden = true
        } else {
            switchBtn.isHidden = false
        }
        
        if isArrowButton != true {
            rightArrowImage.isHidden = true
        } else {
            rightArrowImage.isHidden = false
        }
        
        customNavigationBarView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self).offset(0)
            make.height.equalTo(60)
        }

        iconImage.snp.makeConstraints { (make) in
            make.left.equalTo(customNavigationBarView).offset(10)
            make.centerY.equalTo(customNavigationBarView)
            make.height.width.equalTo(25)
        }
        
        rightArrowImage.snp.makeConstraints { (make) in
            make.right.equalTo(customNavigationBarView).offset(-10)
            make.centerY.equalTo(customNavigationBarView)
            make.width.height.equalTo(30)
//            make.height.equalTo(22)
        }
        
        rightL.snp.makeConstraints { (make) in
            make.right.equalTo(rightArrowImage.snp.left).offset(-5)
            make.centerY.equalTo(rightArrowImage)
        }
        
        labelText.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(10)
            make.right.equalTo(rightL.snp.left).offset(-8)
            make.centerY.equalTo(rightArrowImage)
        }
        
        switchBtn.snp.makeConstraints { (make) in
            make.right.equalTo(customNavigationBarView).offset(-10)
            make.centerY.equalTo(customNavigationBarView)
            make.height.equalTo(30)
            make.width.equalTo(50)
        }
    }
}


class SwitchButton: UIButton {
    var status: Bool = false {
        didSet {
            self.update()
        }
    }
    var onImage = #imageLiteral(resourceName: "light mode-1")
    var offImage = #imageLiteral(resourceName: "light mode")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setStatus(false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update() {
        UIView.transition(with: self, duration: 0.10, options: .transitionCrossDissolve, animations: {
            self.status ? self.setImage(self.onImage, for: .normal) : self.setImage(self.offImage, for: .normal)
        }, completion: nil)
    }
    
    func toggle() {
        if self.status == true {
            self.status = false
            Application.setBoolValueToUserDefaults(false, forKey: C.Stored._darkModeEnabled)
            C.Stored.darkModeEnabled = false
            C.theme.theme = .light
            NotificationCenter.default.post(name: .darkModeDisabled, object: nil)
        } else {
            self.status = true
            Application.setBoolValueToUserDefaults(true, forKey: C.Stored._darkModeEnabled)
            C.Stored.darkModeEnabled = true
            C.theme.theme = .dark
            NotificationCenter.default.post(name: .darkModeEnabled, object: nil)
        }

//        self.status ? self.setStatus(false) : self.setStatus(true)
    }
    
    func setStatus(_ status: Bool) {
        self.status = status
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.sendHapticFeedback()
        self.toggle()
    }
    
    func sendHapticFeedback() {
        let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .heavy)
        impactFeedbackgenerator.prepare()
        impactFeedbackgenerator.impactOccurred()
    }
    
}

struct Recording {
    let fileURL: URL
    let createdAt: String
}


struct ScreenSize {
  static let width = UIScreen.main.bounds.size.width
  static let height = UIScreen.main.bounds.size.height
  static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
  static let maxWH = max(ScreenSize.width, ScreenSize.height)
}

struct DeviceType {
  static let iPhone4orLess = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH < 568.0
  static let iPhone5orSE   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 568.0
  static let iPhone678     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 667.0
  static let iPhone678p    = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 736.0
  static let iPhoneX       = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 812.0
  static let iPhoneXRMax   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 896.0
  static var hasNotch: Bool {
    return iPhoneX || iPhoneXRMax
  }
     
}

class CommonMethods {
  class  func getCreationDate(for file: URL) -> String {
        if let attributes = try? FileManager.default.attributesOfItem(atPath: file.path) as [FileAttributeKey: Any],
            let creationDate = attributes[FileAttributeKey.creationDate] as? Date {
            if let timeResult = creationDate.timeIntervalSinceReferenceDate as? Double {
                let date = Date(timeIntervalSinceReferenceDate: timeResult)
                let dateFormatter = DateFormatter()
                dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                dateFormatter.timeZone = .current
                let localDate = dateFormatter.string(from: date)
                print(localDate)
                return localDate

            }
        } else {
            return ""
        }
    }

  class func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int) ->()) {
            completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

  class func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
}

enum Theme {
    case dark
    case light
}

struct AppTheme {
    var theme: Theme = Theme.light
    
    var bgVColor: UIColor {
        get {
            return ((theme == .light ? (UIColor.white) : (UIColor.init(rgb: 0x121212))))
        }
    }
    
//    var color: UIColor {
//        get {
//            return ((theme == .light ? (C.Stored.Colors.primaryLightColor) : (C.Stored.Colors.primaryDarkColor)))
//        }
//    }

    var navColor: UIColor {
        get {
            return ((theme == .light ? (UIColor.white) : (Application.Colors.primaryDarkColor)))
        }
    }

    var backgroundColor: UIColor {
        get {
            return ((theme == .light ? (UIColor.white) : (UIColor.black)))
        }
    }

    var textColor: UIColor {
        get {
            return ((theme == .light ? (UIColor.black) : (UIColor.white)))
        }
    }
    
    var darkGrayTextColor: UIColor {
        get {
            return ((theme == .light ? (UIColor.darkGray) : (UIColor.lightGray)))
        }
    }

    
    init() {}
    init(_theme: Theme) {
        self.theme = _theme
    }
}


struct C {
    static var theme = AppTheme(_theme: Theme.light)
    struct Stored {
        static let _darkModeEnabled = "_darkModeEnabled"
        static var darkModeEnabled: Bool = false
    }
}

extension Notification.Name {
    static let darkModeEnabled = Notification.Name("com.CricFreak.notifications.darkModeEnabled")
    static let darkModeDisabled = Notification.Name("com.CricFreak.notifications.darkModeDisabled")
}

struct Transcription {
    var countryName = String()
    var countryCode = String()
}
