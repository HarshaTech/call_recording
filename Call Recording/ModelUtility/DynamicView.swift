//
//  DynamicView.swift
//  REAL ESTATE
//
//  Created by TechQuadra on 03/10/19.
//  Copyright © 2019 Vyclean Solutions. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CustomTextField: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageV: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    
    let textF: UITextFieldX = {
        let TF = UITextFieldX()
        TF.font = UIFont.systemFont(ofSize: 16)
        TF.textColor = UIColor.darkGray
        return TF
    }()
    
    let bottonV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.darkGray
        return v
    }()
    
    init(_placeholder: String, image: UIImage) {
        super.init(frame: .zero)
        
        textF.attributedPlaceholder = NSAttributedString(string: _placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .medium)])
        imageV.image = image
        
        addSubview(imageV)
        addSubview(textF)
        addSubview(bottonV)
        
        imageV.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(0)
            make.centerY.equalTo(self)
            make.width.height.equalTo(20)
        }
        
        textF.snp.makeConstraints { (make) in
            make.left.equalTo(imageV.snp.right).offset(10)
            make.centerY.equalTo(imageV)
            make.right.equalTo(self.snp.right).offset(-5)
        }
        
        bottonV.snp.makeConstraints { (make) in
            make.left.equalTo(imageV)
            make.right.equalTo(textF)
            make.height.equalTo(1)
            make.top.equalTo(imageV.snp.bottom).offset(5)
            make.bottom.equalTo(self.snp.bottom).offset(0)
            
        }
    }

}

