//
//  MediaCollectionViewLayout.swift
//  Neeti
//
//  Created by Rutwik on 03/12/18.
//  Copyright © 2018 Ashwinkumar Mangrulkar. All rights reserved.
//

import UIKit
enum MosaicSegmentStyle {
    case fullWidth, fiftyFifty, twoThirdsOneThird, oneThirdTwoThirds, oneThirds
}

class MediaCollectionViewLayout: UICollectionViewLayout {
    var contentBounds = CGRect.zero
    var cachedAttributes = [UICollectionViewLayoutAttributes]()
    //collectionViewContentSize
   
    
    override func prepare() {
        super.prepare()
        
        guard let cv = collectionView else { return }
        
        cachedAttributes.removeAll()
        contentBounds = CGRect.init(origin: .zero, size: cv.bounds.size)
        
//        let count = cv.numberOfItems(inSection: 0)
//        print("MediaCollectionViewLayout.data.count = \(count)")
//
//        var currentIndex = 0
//        var segment: MosaicSegmentStyle = .twoThirdsOneThird
//        var lastFrame: CGRect = .zero
//        var isLeft = true
//
//        let cvW = cv.bounds.size.width
//        let cvH = 200
//        var contentHeight = (1.0/3.0) * cvW
//        print("MediaCollectionViewLayout.cvW = \(cvW)")
//
        let count = cv.numberOfItems(inSection: 0)
//        print("MediaCollectionViewLayout.data.count = \(count)")
        
        var currentIndex = 0
        var segment: MosaicSegmentStyle = .fullWidth//.oneThirds
        var lastFrame: CGRect = .zero
        var isLeft = true
        
        let cvW = 0.75 * cv.bounds.size.width//cv.bounds.size.width
        var contentHeight = cv.bounds.size.height// / 2//(1.0/3.0) * cvW
//        print("MediaCollectionViewLayout.cvW = \(cvW)")
        
        
        
        while currentIndex < count {
            
            let segmentFrame = CGRect(x: lastFrame.maxX, y: 0, width: cvW, height: contentHeight)//CGRect(x: 0, y: 0, width: cvW, height: contentHeight)//CGRect(x: 0, y: lastFrame.maxY + 1.0, width: cvW, height: contentHeight)
            var segmentRects = [segmentFrame]//[CGRect]()
            
//            print("MediaCollectionViewLayout.segmentFrame = \(segmentFrame)")
            let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: currentIndex, section: 0))
            
            attributes.frame = segmentFrame
            cachedAttributes.append(attributes)
            contentBounds = contentBounds.union(lastFrame)
            //contentBounds.size.width += lastFrame.width
            currentIndex += 1
            lastFrame = segmentFrame
        }
        
        /*while currentIndex < count {
            
            let segmentFrame = CGRect(x: 0, y: lastFrame.maxY + 1.0, width: cvW, height: contentHeight)
            var segmentRects = [segmentFrame]//[CGRect]()
            
            print("MediaCollectionViewLayout.segmentFrame = \(segmentFrame)")
         
            
            switch segment {
            case .fullWidth:
                segmentRects = [segmentFrame]
                
            case .fiftyFifty:
                let horizontalSlices = segmentFrame.divided(atDistance: 0.5 * cvW, from: CGRectEdge.minXEdge)//segmentFrame.dividedIntegral(fraction: 0.5, from: .minXEdge)
                segmentRects = [horizontalSlices.slice, horizontalSlices.remainder]
                
            case .twoThirdsOneThird:
               // DispatchQueue.main.async {
                    let horizontalSlices = segmentFrame.divided(atDistance: (2.0/3.0) * cvW, from: CGRectEdge.minXEdge)
//                    let verticalSlices =
//                        horizontalSlices.remainder.divided(atDistance: 0.5 * contentHeight , from: CGRectEdge.minYEdge)//dividedIntegral(fraction: 0.5, from: .minYEdge)
                    segmentRects = [horizontalSlices.slice, horizontalSlices.remainder]//.slice, verticalSlices.remainder]
               // }
                
                
            case .oneThirdTwoThirds:
                //DispatchQueue.main.async {
                    let horizontalSlices = segmentFrame.divided(atDistance: (1.0/3.0) * cvW, from: CGRectEdge.minXEdge)//segmentFrame.dividedIntegral(fraction: (1.0 / 3.0), from: .minXEdge)
                    let verticalSlices = horizontalSlices.slice.divided(atDistance: 0.5 * contentHeight , from: CGRectEdge.minYEdge)//dividedIntegral(fraction: 0.5, from: .minYEdge)
                    segmentRects = [verticalSlices.slice, verticalSlices.remainder, horizontalSlices.remainder]
               // }
                
            case .oneThirds:
                //DispatchQueue.main.async {
                    let horizontalSlices1 = segmentFrame.divided(atDistance: (1.0/3.0) * cvW, from: CGRectEdge.minXEdge)
                    //print("MediaCollectionViewLayout.horizontalSlices1 = \(horizontalSlices1)")
                    let horizontalSlices2 =
                        horizontalSlices1.remainder.divided(atDistance: 0.5 * horizontalSlices1.remainder.size.width , from: CGRectEdge.minXEdge)//dividedIntegral(fraction: 0.5, from: .minYEdge)
                    segmentRects = [horizontalSlices1.slice, horizontalSlices2.slice, horizontalSlices2.remainder]
                //}
                
            }
            
        
         print("MediaCollectionViewLayout.segmentRects = \(segmentRects)")
        for rect in segmentRects {
             print("MediaCollectionViewLayout.rect = \(rect)")
            let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: currentIndex, section: 0))
            
            attributes.frame = rect
            cachedAttributes.append(attributes)
            contentBounds = contentBounds.union(lastFrame)
            
            currentIndex += 1
            lastFrame = rect
        }
        
            // Determine the next segment style.
//            switch count - currentIndex {
//            case 1:
//                segment = .fullWidth
//            case 2:
//                segment = .fiftyFifty
//            default:
//                switch segment {
//                case .fullWidth:
//                    segment = .fiftyFifty
//                case .fiftyFifty:
//                    segment = .twoThirdsOneThird
//                case .twoThirdsOneThird:
//                    isLeft = !isLeft
//                    segment = .oneThirds
//                    contentHeight = (1.0/3.0) * cvW
//
//                    //segment = .oneThirdTwoThirds
//                case .oneThirdTwoThirds:
//                    isLeft = !isLeft
//                    segment = .oneThirds
//                    contentHeight = (1.0/3.0) * cvW
//                    //segment = .fiftyFifty
//                case .oneThirds:
//
//                    segment = isLeft ?.twoThirdsOneThird: .oneThirdTwoThirds
//                    contentHeight = (2.0/3.0) * cvW
//                }
//            }
        }*/
    }
    
    override var collectionViewContentSize: CGSize {
        print("contentBounds.size == \(contentBounds.size)")
        return contentBounds.size
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let cv = collectionView else {
            print("return Guard")
            return  false
            
        }
        return true//!newBounds.size.equalTo(cv.bounds.size)
    }
    
     override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes in cachedAttributes {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        print("visibleLayoutAttributes == \(visibleLayoutAttributes)")
        return visibleLayoutAttributes
     }
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        print("cachedAttributes[indexPath.item] == \(cachedAttributes[indexPath.item])")
        return cachedAttributes[indexPath.item]
    }
}
