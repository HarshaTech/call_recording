//
//  LanguagesVC.swift
//  Call Recording
//
//  Created by TechQuadra on 09/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit
import Alamofire

class LanguagesVC: UIViewController {
    
    var topBarV = CustomNavigationBar(_isBack: true)
    var customNavigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = C.theme.backgroundColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()

    var selectedLangL: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.red
        l.text = "English (India)"
        l.font = UIFont(name: "Montserrat-Regular", size: 14)
        return l
    }()
    
    var otherLangL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.darkGrayTextColor
        l.text = "Other Languages"
        l.font = UIFont(name: "Montserrat-Regular", size: 14)
        return l
    }()
    
    var tableBarV: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = C.theme.backgroundColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()

    let tableV: UITableView = {
        let table = UITableView()
        table.backgroundColor = C.theme.backgroundColor
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.showsVerticalScrollIndicator = false
        return table
    }()
    
    var index = Int()

    var langArray: [Transcription] = [Transcription(countryName: "Arabic (Saudi Arabia)", countryCode: "ar-SA"),Transcription(countryName: "Arabic (United Arab Emirates)", countryCode: "ar-AE"),Transcription(countryName: "Chinese( Mainland)", countryCode: "yue-Hant-HK"),Transcription(countryName: "English (United Kingdom)", countryCode: "en-GB"),Transcription(countryName: "English (Australia)", countryCode: "en-AU"),Transcription(countryName: "English (Canada)", countryCode: "en-CA"),Transcription(countryName: "English (Hong Kong)", countryCode: "en-HK"),Transcription(countryName: "English (India)", countryCode: "en-IN"),Transcription(countryName: "English (Singapore)", countryCode: "en-SG"),Transcription(countryName: "English (United States)", countryCode: "en-US"),Transcription(countryName: "French (Canada)", countryCode: "fr-CA"),Transcription(countryName: "French (France)", countryCode: "fr-FR"),Transcription(countryName: "German (Germany)", countryCode: "de-DE"),Transcription(countryName: "German (Switzerland)", countryCode: "de-CH"),Transcription(countryName: "Hindi (India)", countryCode: "hi-IN"),Transcription(countryName: "Indonesian (Indonesia)", countryCode: "id-ID"),Transcription(countryName: "Japanese (Japan)", countryCode: "ja-JP"),Transcription(countryName: "Korean (South Korea)", countryCode: "ko-KR"),Transcription(countryName: "Malay (Malaysia)", countryCode: "ms-MY"),Transcription(countryName: "Spanish (Spain)", countryCode: "es-ES"),Transcription(countryName: "Spanish (United States)", countryCode: "es-US"),Transcription(countryName: "Tamil (India)", countryCode: "ta-IN"),Transcription(countryName: "Turkish (Turkey)", countryCode: "tr-TR"),Transcription(countryName: "Urdu (India)", countryCode: "ur-IN"),Transcription(countryName: "Vietnamese (Vietnam)", countryCode: "vi-VN")]

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let lang = Application.getValueAsStringFromUserDefaults(forKey: Application.Stored.selectCountry)
        for (index, each) in langArray.enumerated() {
            if each.countryName == lang {
                self.index = index
            }
        }
    }
    
    func initialSetUp() {
        view.backgroundColor = C.theme.backgroundColor
        topBarV.titleL.text = "Transcription Language"
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(LanguageCell.self, forCellReuseIdentifier: LanguageCell.identifier)
        topBarV.backButton.addTarget(self, action: #selector(backBtnClicked), for: .touchUpInside)
    }
    
    func setUpViews() {
        view.addSubview(topBarV)
        view.addSubview(customNavigationBarView)
        customNavigationBarView.addSubview(selectedLangL)
        view.addSubview(otherLangL)
        view.addSubview(tableBarV)
        view.addSubview(tableV)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(90)
            } else {
                make.height.equalTo(70)
            }
        }
        
        customNavigationBarView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.top.equalTo(topBarV.snp.bottom).offset(10)
            make.height.equalTo(50)
        }
        
        selectedLangL.snp.makeConstraints { (make) in
            make.left.equalTo(customNavigationBarView).offset(10)
            make.centerY.equalTo(customNavigationBarView)
        }
        
        otherLangL.snp.makeConstraints { (make) in
            make.left.equalTo(customNavigationBarView).offset(10)
            make.top.equalTo(customNavigationBarView.snp.bottom).offset(8)
        }
        
        tableBarV.snp.makeConstraints { (make) in
            make.left.right.equalTo(customNavigationBarView)
            make.top.equalTo(otherLangL.snp.bottom).offset(10)
            make.bottom.equalTo(view)
        }
        
        tableV.snp.makeConstraints { (make) in
            make.left.top.equalTo(tableBarV).offset(5)
            make.bottom.right.equalTo(tableBarV).offset(-5)
        }
    }
    
    @objc func backBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension LanguagesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return langArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LanguageCell.identifier) as! LanguageCell
        cell.populate(data: langArray[indexPath.row].countryName, indexVal: indexPath.row, indexStr: index)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        index = indexPath.row
        Application.setValueToUserDefaults(langArray[indexPath.row].countryName, forKey: Application.Stored.selectCountry)
        Application.setValueToUserDefaults(langArray[indexPath.row].countryCode, forKey: Application.Stored.selectCode)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


class LanguageCell: UITableViewCell {
    
    var bgV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        return v
    }()
    
    var textL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 15)
        return l
    }()
    
    var separatorV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        return v
    }()
    
    
    var checkBtn: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate(data: String, indexVal: Int, indexStr: Int)  {
        self.backgroundColor = UIColor.clear
        self.addSubview(bgV)
        bgV.addSubview(textL)
        bgV.addSubview(separatorV)
        bgV.addSubview(checkBtn)
        
        self.backgroundColor = C.theme.backgroundColor
        textL.text = data
        
        if indexVal == indexStr {
             checkBtn.image = #imageLiteral(resourceName: "tick")
        } else {
             checkBtn.image = UIImage(named: "")
        }
        
        bgV.snp.makeConstraints { (make) in
            make.left.top.equalTo(self).offset(0)
            make.right.equalTo(self).offset(0)
            make.bottom.equalTo(self)
            make.height.equalTo(50)
        }
        
        textL.snp.makeConstraints { (make) in
            make.left.equalTo(bgV).offset(8)
            make.centerY.equalTo(bgV)
        }
        
        checkBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(bgV)
            make.right.equalTo(bgV).offset(-10)
            make.height.width.equalTo(18)
        }
        
        separatorV.snp.makeConstraints { (make) in
            make.left.right.equalTo(bgV)
            make.height.equalTo(1)
            make.bottom.equalTo(bgV).offset(-5)
        }
    }
}
