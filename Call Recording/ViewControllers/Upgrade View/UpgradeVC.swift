//
//  UpgradeVC.swift
//  Call Recording
//
//  Created by TechQuadra on 10/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class UpgradeVC: UIViewController {
    
    var scrollV: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bouncesZoom = false
        scroll.backgroundColor = UIColor.clear
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()

    var bgImage: UIImageView = {
        let bg = UIImageView()
        bg.image = #imageLiteral(resourceName: "start trial")
        bg.backgroundColor = UIColor.black
        bg.alpha = 0.13
        bg.contentMode = .scaleAspectFill
        bg.clipsToBounds = true
        return bg
    }()
    
    var closeBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "cancel").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = C.theme.textColor
        return btn
    }()
    
    var upgradeL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.textAlignment = .center
        l.font = UIFont(name: "Montserrat-SemiBold", size: 23)
        l.text = "Upgrade to Premium"
        return l
    }()
    
    var topL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.textAlignment = .center
        l.numberOfLines = 2
//        l.font = UIFont(name: "", size: 18)
//        l.text = "Upgrade to Premium"
        return l
    }()
    
    var trialL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.numberOfLines = 0
        l.textAlignment = .center
//        l.font = UIFont(name: "", size: 18)
//        l.text = "Upgrade to Premium"
        return l
    }()

    var supportL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.textAlignment = .center
        l.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        l.text = "Supported languages"
        return l
    }()
    
    var continueBtn: UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(#imageLiteral(resourceName: "splash btn"), for: .normal)
        btn.setTitleColor(C.theme.textColor, for: .normal)
        btn.setTitle("Start & Trial Plan", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 16)
        return btn
    }()

    var termsBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Terms of Use", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        btn.setTitleColor(C.theme.darkGrayTextColor, for: .normal)
        return btn
    }()

    var privacyBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Privacy Policy", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        btn.setTitleColor(C.theme.darkGrayTextColor, for: .normal)
        return btn
    }()
    
    var infoCollectionV: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .red
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        return cv
    }()

    var langArray = ["English", "Japanese", "Korean", "Chinese", "French", "Spanish", "Tamil", "Hindi", "Indonesian", "Arabic", "German", "Malay", "Turkish"]
    
    let yourAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 14),
                                                         .foregroundColor: C.theme.textColor, .underlineStyle: NSUnderlineStyle.single.rawValue]

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setupViews()
    }
    
    func initialSetUp()  {
        self.view.backgroundColor = UIColor.black
        let attributeString1 = NSMutableAttributedString(string: "Terms of Use",attributes: yourAttributes)
        termsBtn.setAttributedTitle(attributeString1, for: .normal)

        let attributeString2 = NSMutableAttributedString(string: "Privacy Policy",attributes: yourAttributes)
        privacyBtn.setAttributedTitle(attributeString2, for: .normal)
        topL.attributedText = Application.attributedTextWith(text1: "to get", text2: " Transcription\n", font1: UIFont(name: "Montserrat-Regular", size: 16) ?? UIFont(), font2: UIFont(name: "Montserrat-SemiBold", size: 16) ?? UIFont(), color1: C.theme.textColor, color2: C.theme.textColor, text3: "and Call Reccordings", font3: UIFont(name: "Montserrat-Regular", size: 16) ?? UIFont(), color3: C.theme.textColor)
        trialL.attributedText = Application.attributedTextWith(text1: "3 days free trial, \nthen auto-renews at ", text2: "₹ 5,900", font1: UIFont(name: "Montserrat-Regular", size: 16) ?? UIFont(), font2: UIFont(name: "Montserrat-SemiBold", size: 16) ?? UIFont(), color1: C.theme.textColor, color2: C.theme.textColor, text3: "/year", font3: UIFont(name: "Montserrat-Regular", size: 16) ?? UIFont(), color3: C.theme.textColor)
        infoCollectionV.register(LanguageCollectioViewCell.self, forCellWithReuseIdentifier: LanguageCollectioViewCell.identifier)
        infoCollectionV.delegate = self
        infoCollectionV.dataSource = self
        closeBtn.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
    }

    func setupViews()  {
        view.addSubview(bgImage)
        view.addSubview(scrollV)
        scrollV.addSubview(closeBtn)
        scrollV.addSubview(upgradeL)
        scrollV.addSubview(topL)
        scrollV.addSubview(trialL)
        scrollV.addSubview(supportL)
        scrollV.addSubview(termsBtn)
        scrollV.addSubview(continueBtn)
        scrollV.addSubview(privacyBtn)
        scrollV.addSubview(infoCollectionV)
        
        bgImage.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        scrollV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        closeBtn.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-15)
            if DeviceType.hasNotch {
                make.top.equalTo(scrollV).offset(35)

            } else {
                make.top.equalTo(scrollV).offset(25)

            }
            make.height.width.equalTo(20)
        }
        
        upgradeL.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.top.equalTo(closeBtn.snp.bottom).offset(20)
        }
        
        topL.snp.makeConstraints { (make) in
            make.width.equalTo(280)
            make.centerX.equalTo(view)
            make.top.equalTo(upgradeL.snp.bottom).offset(20)
        }
        
        trialL.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(30)
            make.right.equalTo(view).offset(-30)
            make.top.equalTo(topL.snp.bottom).offset(20)
        }
        
        supportL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(trialL.snp.bottom).offset(35)
        }
        
        infoCollectionV.snp.makeConstraints { (make) in
            make.top.equalTo(supportL.snp.bottom).offset(10)
            make.centerX.equalTo(view).offset(10)
            make.height.equalTo(280)
            make.width.equalTo(200)
        }
        
        continueBtn.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.centerX.equalTo(view)
            make.top.equalTo(infoCollectionV.snp.bottom).offset(20)
            make.height.equalTo(60)
        }
        
        termsBtn.snp.makeConstraints { (make) in
            make.top.equalTo(continueBtn.snp.bottom).offset(15)
            make.centerX.equalTo(view)
            make.height.equalTo(20)
        }
        
        privacyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(termsBtn.snp.bottom).offset(5)
            make.centerX.equalTo(view)
            make.bottom.equalTo(scrollV.snp.bottom).offset(-20)
            make.bottom.equalTo(scrollV.snp.bottom).offset(-20)
        }
    }
    
    @objc func dismissView() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UpgradeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return langArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LanguageCollectioViewCell.identifier, for: indexPath) as! LanguageCollectioViewCell
        cell.populate(data: langArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 5, height: 30)
    }
}

class LanguageCollectioViewCell: UICollectionViewCell {

    let bgv: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }()
    
    var pointV: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 4
        v.backgroundColor = UIColor.red
        return v
    }()
    
    var textL: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.white
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        return l
    }()
    
    func populate(data: String)  {
        self.addSubview(bgv)
        bgv.addSubview(pointV)
        bgv.addSubview(textL)
        
        textL.text = data
        
        bgv.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(self).offset(2)
            make.height.equalTo(20)
        }
        
        pointV.snp.makeConstraints { (make) in
            make.centerY.left.equalTo(bgv)
            make.width.height.equalTo(8)
        }
        
        textL.snp.makeConstraints { (make) in
            make.left.equalTo(pointV.snp.right).offset(4)
            make.centerY.equalTo(bgv)
        }
    }
}
