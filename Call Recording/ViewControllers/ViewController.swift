//
//  ViewController.swift
//  Call Recording
//
//  Created by TechQuadra on 01/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imageV: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "splash bg")
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    
    var nextBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.init(rgb: 0xF01332)
        btn.layer.cornerRadius = 5
        btn.setTitle("Continue", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        return btn
    }()
    
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
//        App.activityIndicator.startAnimating()
//        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerClicked), userInfo: nil, repeats: false)
        // Do any additional setup after loading the view.
    }
    
    func setUpViews() {
        self.view.backgroundColor = C.theme.backgroundColor
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.addSubview(imageV)
        view.addSubview(nextBtn)
        nextBtn.addTarget(self, action: #selector(goToNextView), for: .touchUpInside)
        
        imageV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        nextBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(40)
                make.width.equalTo(200)
                make.bottom.equalTo(view).offset(-90)
            } else {
                make.height.equalTo(35)
                make.width.equalTo(160)
                make.bottom.equalTo(view).offset(-60)
            }
        }
        view.addSubview(App.activityIndicator)
    }

    @objc func goToNextView() {
        let VC = PrivacyViewController()
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @objc func timerClicked() {
        App.activityIndicator.stopAnimating()
    }

}

