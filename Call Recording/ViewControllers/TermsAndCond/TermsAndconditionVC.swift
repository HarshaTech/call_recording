//
//  TermsAndconditionVC.swift
//  Call Recording
//
//  Created by TechQuadra on 29/07/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit
import WebKit


class TermsAndconditionVC: UIViewController, WKNavigationDelegate {
    
    var topBarV = CustomNavigationBar(_isBack: true)
    var btmV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.groupTableViewBackground
        return v
    }()

    let webV: WKWebView = {
        let wv = WKWebView()
        return wv
    }()
    
    var titleStr = String()
    var urlStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        view.backgroundColor = UIColor.white
        topBarV.titleL.text = titleStr
        webV.navigationDelegate = self
        let url = URL(string: urlStr)!
        webV.load(URLRequest(url: url))
        webV.allowsBackForwardNavigationGestures = true
        self.topBarV.backButton.addTarget(self, action: #selector(backbtnClicked), for: .touchUpInside)

    }
    
    func setUpView()  {
        view.addSubview(topBarV)
        view.addSubview(btmV)
        view.addSubview(webV)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(90)
            } else {
                make.height.equalTo(70)
            }
        }
        
        btmV.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(topBarV.snp.bottom)
            make.height.equalTo(1)
        }
        
        webV.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(view)
            make.top.equalTo(btmV.snp.bottom).offset(0)
        }
    }
    
    @objc func backbtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }

}
