//
//  VoiceRecordingListVC.swift
//  Call Recording
//
//  Created by TechQuadra on 03/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit
import SnapKit
import AVFoundation
import Alamofire

class VoiceRecordingListVC: UIViewController {
    
    var topBarV = CustomNavigationBar(_isBack: true)
    var noRecordsL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.text = "No Recordings Found"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 20)
        return l
    }()
    
    let dirs : [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
    
    let tableV: UITableView = {
        let table = UITableView()
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.showsVerticalScrollIndicator = false
        return table
    }()

    var bgV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor.withAlphaComponent(0.6)
        v.isHidden = true
        return v
    }()
    
    var smallPopUp: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 5
        v.clipsToBounds = true
        return v
    }()
    
    var newNameL: UILabel = {
        let l = UILabel()
        l.text = "New Name"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        return l
    }()
    
    var nameTF: UITextFieldX = {
        let tf = UITextFieldX()
        tf.borderStyle = .none
        tf.maxLength = 15
        tf.textColor = C.theme.textColor
        return tf
    }()
    
    var bottomV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.black
        return v
    }()
    
    var cancelBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Cancel", for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        return btn
    }()
    
    var saveBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Save", for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        return btn
    }()

    var playerBGV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        v.isHidden = true
        return v
    }()
    
    var playerView: UIView = {
        let v = UIView()
        v.layer.borderColor = UIColor.lightGray.cgColor
        v.layer.borderWidth = 1
        v.layer.cornerRadius = 15
        v.backgroundColor = UIColor.white
        return v
    }()
    
    var closeBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "cancel").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = C.theme.textColor
        return btn
    }()
    
    var timerL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        l.text = "00:00"
        return l
    }()
    
    var nameL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        l.text = "Record"
//        l.backgroundColor = UIColor.red
        return l
    }()
    
    var initialTimeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 16)
        l.textAlignment = .left
        l.text = "00:00"
        return l
    }()
    
    var endTimeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        l.textAlignment = .right
        l.text = "00:00"
        return l
    }()

    var editBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-edit-24"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    var previousBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-end-31 copy"), for: .normal)
        return btn
    }()

    var nextBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-end-31"), for: .normal)
        return btn
    }()

    var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-delete-bin-24"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit

        return btn
    }()

    var stopBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-rounded-square-24"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()

    var playBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-play-30"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit

        return btn
    }()
    
    var btnStackV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 30
        return sv
    }()

    var slider: UISlider = {
        var sl = UISlider()
        sl.thumbTintColor = UIColor.init(rgb: 0xf01332)
        sl.tintColor = UIColor.init(rgb: 0xf01332)
        return sl
    }()

    var transcrptV: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 23
        v.layer.borderColor = UIColor.init(rgb: 0xf01332).cgColor
        v.layer.borderWidth = 1
        return v
    }()
    
    var transcriptImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "transcription lang")
        return image
    }()
    
    var transCriptLabel: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.text = "Transcribe"
        l.textAlignment = .center
        l.font = UIFont(name: "Montserrat-Semibold", size: 12)
        return l
    }()
    
    var transcribBtn: UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    var tag = Int()
    var audioPlayer:AVAudioPlayer!
    var avPlayer:AVPlayer?
    var avPlayerItem:AVPlayerItem?
    var timer = Timer()
    var playing = false
    var recordings = [Recording]()
    var durationArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        initialSetUpV()
        setUpViews()
        getRecords()
        // Do any additional setup after loading the view.
    }
    
    func setUpViews()  {
        view.addSubview(topBarV)
        view.addSubview(tableV)
        view.addSubview(noRecordsL)
        
        view.addSubview(playerBGV)
        playerBGV.addSubview(playerView)
        playerView.addSubview(closeBtn)
        playerView.addSubview(timerL)
        playerView.addSubview(initialTimeL)
        playerView.addSubview(endTimeL)
        playerView.addSubview(nameL)
        playerView.addSubview(editBtn)
        playerView.addSubview(btnStackV)
        btnStackV.addArrangedSubview(previousBtn)
        playerView.addSubview(slider)
        btnStackV.addArrangedSubview(deleteBtn)
        btnStackV.addArrangedSubview(playBtn)
        btnStackV.addArrangedSubview(stopBtn)
        btnStackV.addArrangedSubview(nextBtn)
        
        view.addSubview(bgV)
        bgV.addSubview(smallPopUp)
        smallPopUp.addSubview(newNameL)
        smallPopUp.addSubview(nameTF)
        smallPopUp.addSubview(bottomV)
        smallPopUp.addSubview(saveBtn)
        smallPopUp.addSubview(cancelBtn)
        
        playerView.addSubview(transcrptV)
        transcrptV.addSubview(transcriptImage)
        transcrptV.addSubview(transCriptLabel)
        transcrptV.addSubview(transcribBtn)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(90)
            } else {
                make.height.equalTo(70)
            }
        }
        
        tableV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(10)
            make.right.equalTo(view).offset(-10)
            make.top.equalTo(topBarV.snp.bottom).offset(10)
            make.bottom.equalTo(view).offset(-10)
        }
        
        noRecordsL.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(view)
        }
        
        bgV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        smallPopUp.snp.makeConstraints { (make) in
            make.centerY.equalTo(view)
            make.left.equalTo(bgV).offset(20)
            make.right.equalTo(bgV).offset(-20)
        }
        
        newNameL.snp.makeConstraints { (make) in
            make.left.top.equalTo(smallPopUp).offset(20)
            make.right.equalTo(smallPopUp).offset(-20)
        }
        
        nameTF.snp.makeConstraints { (make) in
            make.left.right.equalTo(newNameL)
            make.height.equalTo(40)
            make.top.equalTo(newNameL.snp.bottom).offset(10)
        }
        
        bottomV.snp.makeConstraints { (make) in
            make.left.right.equalTo(nameTF)
            make.top.equalTo(nameTF.snp.bottom).offset(2)
            make.height.equalTo(1)
        }
        
        saveBtn.snp.makeConstraints { (make) in
            make.right.equalTo(nameTF)
            make.top.equalTo(bottomV.snp.bottom).offset(30)
            make.width.equalTo(100)
            make.bottom.equalTo(smallPopUp.snp.bottom).offset(-25)

        }
        
        cancelBtn.snp.makeConstraints { (make) in
            make.right.equalTo(saveBtn.snp.left).offset(-10)
            make.top.bottom.width.equalTo(saveBtn)
        }
        
        playerBGV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        playerView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(10)
            make.right.equalTo(view).offset(-10)
            make.bottom.equalTo(view).offset(-20)
            make.height.equalTo(300)
        }
        
        closeBtn.snp.makeConstraints { (make) in
            make.right.equalTo(playerView).offset(-15)
            make.top.equalTo(playerView).offset(15)
            make.height.width.equalTo(15)
        }
        
        timerL.snp.makeConstraints { (make) in
            make.centerY.equalTo(closeBtn)
            make.centerX.equalTo(playerView)
            make.width.equalTo(100)
        }
        
        nameL.snp.makeConstraints { (make) in
            make.centerX.equalTo(playerView)
            make.top.equalTo(timerL.snp.bottom).offset(30)
        }
        
        editBtn.snp.makeConstraints { (make) in
            make.left.equalTo(nameL.snp.right).offset(5)
            make.centerY.equalTo(nameL)
            make.height.width.equalTo(20)
        }
        
        endTimeL.snp.makeConstraints { (make) in
            make.right.equalTo(closeBtn)
            make.centerY.equalTo(editBtn)
        }
        
        initialTimeL.snp.makeConstraints { (make) in
            make.left.equalTo(playerView).offset(15)
            make.centerY.equalTo(editBtn)
        }
        
        slider.snp.makeConstraints { (make) in
            make.left.equalTo(initialTimeL)
            make.right.equalTo(endTimeL)
            make.top.equalTo(editBtn.snp.bottom).offset(25)
        }
        
        btnStackV.snp.makeConstraints { (make) in
            make.left.right.equalTo(slider)
            make.top.equalTo(slider.snp.bottom).offset(20)
            make.height.equalTo(35)
        }
        
        nextBtn.snp.makeConstraints { (make) in
            make.height.width.equalTo(14)
        }
        
        stopBtn.snp.makeConstraints { (make) in
            make.height.width.equalTo(14)
        }
        
        transcrptV.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.height.equalTo(45)
            make.width.equalTo(140)
            make.bottom.equalTo(playerView).offset(-15)
        }
        
        transcriptImage.snp.makeConstraints { (make) in
            make.left.equalTo(transcrptV).offset(15)
            make.centerY.equalTo(transcrptV)
            make.width.height.equalTo(24)
        }
        
        transCriptLabel.snp.makeConstraints { (make) in
            make.left.equalTo(transcriptImage.snp.right)
            make.right.equalTo(transcrptV)
            make.centerY.equalTo(transcrptV)
        }
        
        transcribBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(transcrptV)
        }

    }
    
    func initialSetUpV()  {
        
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)

        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = C.theme.backgroundColor
        topBarV.titleL.text = "Voice Recordings"
        tableV.register(RecordingListTVV.self, forCellReuseIdentifier: RecordingListTVV.identifier)
        tableV.delegate = self
        tableV.dataSource = self
        topBarV.backButton.addTarget(self, action: #selector(backBtnClicked), for: .touchUpInside)
        saveBtn.addTarget(self, action: #selector(saveBtnClicked), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
        playBtn.addTarget(self, action: #selector(playBtnAction), for: .touchUpInside)
        nextBtn.addTarget(self, action: #selector(nextAudioPlayer), for: .touchUpInside)
        previousBtn.addTarget(self, action: #selector(previousBtnClicked), for: .touchUpInside)
        deleteBtn.addTarget(self, action: #selector(deleteAlert), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(closeBtnAction), for: .touchUpInside)
        editBtn.addTarget(self, action: #selector(audioFileNameUpdate), for: .touchUpInside)
        stopBtn.addTarget(self, action: #selector(stopBtnAction), for: .touchUpInside)
        transcribBtn.addTarget(self, action: #selector(transcriptClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.view.backgroundColor = C.theme.backgroundColor
        self.topBarV.backgroundColor = C.theme.navColor
        
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.view.backgroundColor = .white
        self.topBarV.backgroundColor = C.theme.navColor
    }
    
    @objc func backBtnClicked() {
        Application.Stored.isBackToAudio = true
        self.navigationController?.popViewController(animated: true)
    }
    
    // get all Recordings
    func getRecords()  {
        self.recordings.removeAll()
        self.durationArray.removeAll()
          let fileManager = FileManager.default
          let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
          let directoryContents = try! fileManager.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil)
        for (index, audio) in directoryContents.enumerated() {
            let recording = Recording(fileURL: audio, createdAt: CommonMethods.getCreationDate(for: audio))
            recordings.append(recording)
            audioPlayer = nil
            do {
                tag = index
                audioPlayer = try AVAudioPlayer(contentsOf: getFilePath())
                self.durationArray.append(audioPlayer.duration.stringFromTimeInterval())
            } catch {
                self.durationArray.append("")
            }
          }
        
    
            tag = 0
            recordings.sort(by: { $0.createdAt.compare($1.createdAt) == .orderedDescending})
            print(recordings)
            if recordings.count == 0 {
                tableV.isHidden = true
                noRecordsL.isHidden = false
            } else {
                tableV.isHidden = false
                noRecordsL.isHidden = true
                self.tableV.reloadData()
            }
    }

    @objc func deleteBtnAction(_ sender: UIButton)  {
        
        Application.showAlertWithActionsTwo(title: "Are you sure you want to delete this file?", subTitle: "", message: "", actionTitle: "Yes", actionTitle2: "No", completion: { (alert) in
            self.present(alert, animated: true, completion: nil)
        }, firstAction: { (firstAction) in
            self.deleteFiles(sender)
        }) { (cancel) in
            
        }
    }
    
    func deleteFiles(_ sender: UIButton) {
       do {
            let fileManager = FileManager.default
           
           // Check if file exists
        if fileManager.fileExists(atPath: recordings[sender.tag].fileURL.path) {
               // Delete file
               try fileManager.removeItem(atPath: recordings[sender.tag].fileURL.path)
            self.getRecords()
            
           } else {
               print("File does not exist")
           }
       }
       catch let error as NSError {
           print("An error took place: \(error)")
       }
    }
    
    @objc func shareBtnClicked(_ sender: UIButton)  {
        let urlVal = URL(string: "\(self.recordings[sender.tag].fileURL)")
        let url = [urlVal]
        let avc = UIActivityViewController(activityItems: url, applicationActivities: nil)
        self.present(avc, animated: true)
    }
    
    @objc func editAudiofile(_ sender: UIButton) {
         let val = self.recordings[sender.tag].fileURL.lastPathComponent.components(separatedBy: ".")
        self.nameTF.text = val[0]
        self.tag = sender.tag
        let trans = CGAffineTransform(translationX: view.frame.width, y: 0)
        self.bgV.transform = trans
        UIView.animate(withDuration: 1.0, animations: {
            self.bgV.isHidden = false
            self.bgV.transform = .identity
        }) { (true) in
            
        }
    }
    
    @objc func audioFileNameUpdate() {
        let val = self.recordings[tag].fileURL.lastPathComponent.components(separatedBy: ".")
        self.nameTF.text = val[0]
        let trans = CGAffineTransform(translationX: view.frame.width, y: 0)
        self.bgV.transform = trans
        UIView.animate(withDuration: 1.0, animations: {
            self.bgV.isHidden = false
            self.bgV.transform = .identity
        }) { (true) in
            
        }

    }
    
   @objc func saveBtnClicked() {
        var urlStr: URL = self.recordings[tag].fileURL
        urlStr.deleteLastPathComponent()
        urlStr.appendPathComponent(nameTF.text! + ".m4a" )
        do {
            try FileManager.default.moveItem(atPath: self.recordings[tag].fileURL.path, toPath: urlStr.path)
            self.cancelBtnClicked()
            self.getRecords()
          } catch {
            print(error.localizedDescription)
            Application.showAlertWithActions(title: error.localizedDescription, subTitle: "", message: "", actionTitle: "Ok", completion: { (alert) in
                self.present(alert, animated: true, completion: nil)
            }) { (firstActiom) in
                self.cancelBtnClicked()
            }
          }
        print(urlStr)
    }
    
    @objc func cancelBtnClicked() {
        self.bgV.isHidden = true
    }
    
    func cacheData() -> URL {
        let fm = FileManager.default
        let docs = try! fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return docs
    }
    
    func getFilePath() -> URL {
        let path = cacheData()
        let filePath = path.appendingPathComponent("\(recordings[tag].fileURL.lastPathComponent)")
        return filePath
    }
    
    @objc func playBtnAction() {
        if audioPlayer == nil {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: getFilePath())
                self.endTimeL.text = audioPlayer.duration.stringFromTimeInterval()
                slider.maximumValue = Float(audioPlayer.duration)
                let image = #imageLiteral(resourceName: "icons8-pause-30").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
                playBtn.tintColor = UIColor.init(rgb: 0xf01332)
                playBtn.setImage(image, for: .normal)
                audioPlayer.delegate = self
                audioPlayer.prepareToPlay()
                audioPlayer.volume = 10.0
                audioPlayer.play()
                timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSliderVal), userInfo: nil, repeats: true)
            } catch {
                print("audio file error")
            }
            avPlayer?.play()

        } else {
            if audioPlayer.isPlaying {
                let image = #imageLiteral(resourceName: "icons8-play-30").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
                playBtn.tintColor = UIColor.init(rgb: 0xf01332)
                playBtn.setImage(image, for: .normal)
                audioPlayer.pause()
                timer.invalidate()
            } else {
                playBtn.setImage(#imageLiteral(resourceName: "icons8-pause-30"), for: .normal)
                audioPlayer.play()
                timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSliderVal), userInfo: nil, repeats: true)
            }
        }
    }
    
    @objc func updateSliderVal()  {
        slider.value = Float(audioPlayer.currentTime)
        self.timerL.text = audioPlayer.currentTime.stringFromTimeInterval()
    }
    
    @objc func nextAudioPlayer() {
        if tag < recordings.count  {
            self.audioPlayer = nil
            self.tag = tag + 1
            self.playBtnAction()
        }
    }
    
    @objc func previousBtnClicked() {
        if tag > 0 {
            self.audioPlayer = nil
            self.tag = tag - 1
            self.playBtnAction()
        }
    }
    
    @objc func deleteAlert() {
        Application.showAlertWithActionsTwo(title: "Are you sure you want to delete this file?", subTitle: "", message: "", actionTitle: "Yes", actionTitle2: "No", completion: { (alert) in
            self.present(alert, animated: true, completion: nil)
        }, firstAction: { (firstAction) in
            self.deleteAudio()
        }) { (cancel) in
            
        }

    }
    
    @objc func deleteAudio() {
        do {
             let fileManager = FileManager.default
            
            // Check if file exists
         if fileManager.fileExists(atPath: recordings[tag].fileURL.path) {
                // Delete file
            try fileManager.removeItem(atPath: recordings[tag].fileURL.path)
            self.getRecords()
            slider.value = 0
            timer.invalidate()
            self.playerBGV.isHidden = true
            self.endTimeL.text = "00:00"
            self.audioPlayer.stop()
            self.audioPlayer = nil
//            self.playerView.isHidden = true
            } else {
                print("File does not exist")
            }
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }

    }
    
    @objc func stopBtnAction() {
        timer.invalidate()
        audioPlayer.stop()
        audioPlayer = nil
        self.playerBGV.isHidden = true
    }
    
    @objc func closeBtnAction() {
        slider.value = 0
        timer.invalidate()
        self.playerBGV.isHidden = true
        self.endTimeL.text = "00:00"
        self.audioPlayer.stop()
        self.audioPlayer = nil
    }
    
    @objc func transcriptClicked(_ sender: UIButton) {
        
        let val = Application.getValueAsStringFromUserDefaults(forKey: Application.Stored.selectCode)
        
        let url = "http://18.221.127.228/speechtotext_api/processdata/convertspeechtotext/"
        let request = URLRequest(url: NSURL(string: url)! as URL)
        let param = ["lang": val]
        if let videoData = NSData(contentsOf: recordings[sender.tag].fileURL) {
            upload(image: videoData as Data, to: request as URLRequestConvertible, params: param)
        }
    }
        
}


extension VoiceRecordingListVC: AVAudioRecorderDelegate, AVAudioPlayerDelegate  {
    //MARK: Delegates
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        slider.value = 0
        timer.invalidate()
        self.playerBGV.isHidden = true
        self.endTimeL.text = "00:00"
        self.audioPlayer.stop()
        self.audioPlayer = nil
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    func upload(image: Data, to url: Alamofire.URLRequestConvertible, params: [String: String]) {
      
        
        AF.upload(multipartFormData: { MultipartFormData in


           MultipartFormData.append(image, withName: "file" , fileName: "image.mov" , mimeType: "video/mov")
                  for(key,value) in params {
                    MultipartFormData.append("\(value)".data(using: .utf8, allowLossyConversion: false)!, withName: key)
                  }
              }, to: "http://18.221.127.228/speechtotext_api/processdata/convertspeechtotext/", method: .post, headers: nil)
         .responseJSON { (response) in
                  debugPrint("SUCCESS RESPONSE: \(response)")
         }
       }
}

extension VoiceRecordingListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableV.dequeueReusableCell(withIdentifier: RecordingListTVV.identifier) as! RecordingListTVV
        cell.populate(titleStr: recordings[indexPath.row].fileURL.lastPathComponent, dateTime: recordings[indexPath.row].createdAt, timeVal: "")
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(_:)), for: .touchUpInside)
        cell.shareBtn.tag = indexPath.row
        cell.shareBtn.addTarget(self, action: #selector(shareBtnClicked(_:)), for: .touchUpInside)
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editAudiofile(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tag = indexPath.row
        self.audioPlayer = nil
        let val = recordings[indexPath.row].fileURL.lastPathComponent.components(separatedBy: ".")
        self.nameL.text = val[0]
        self.playerBGV.isHidden = false
        transcribBtn.tag = indexPath.row
            
        
        playBtnAction()
    }
}


class RecordingListTVV: UITableViewCell {
    
    var bgV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        return v
    }()
    
    var recordingImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "mic 5")
        image.clipsToBounds = true
        return image
    }()
    
    var shareBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        return btn
    }()
    
    var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        return btn
    }()

    var editBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
        return btn
    }()
    
    var btnStackV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 7
        return sv
    }()
    
    var titleL: UILabel = {
        let l = UILabel()
        l.text = "Audio"
        l.font = UIFont(name: "Montserrat-Regular", size: 18)
        l.textColor = C.theme.textColor
        return l
    }()

    var timeL: UILabel = {
        let l = UILabel()
        l.text = ""
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        l.textColor = C.theme.darkGrayTextColor
        return l
    }()

    var dateL: UILabel = {
        let l = UILabel()
        l.text = ""
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        l.textColor = C.theme.darkGrayTextColor
        return l
    }()
    
    var bottomSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillProportionally
        sv.spacing = 10
        return sv
    }()
    
    var labelSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 2
        sv.distribution = .fillProportionally
        return sv
    }()

    var separatorV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        return v
        
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate(titleStr: String, dateTime: String, timeVal: String) {
        self.addSubview(bgV)
        bgV.addSubview(recordingImage)
        bgV.addSubview(btnStackV)
        btnStackV.addArrangedSubview(editBtn)
        btnStackV.addArrangedSubview(shareBtn)
        btnStackV.addArrangedSubview(deleteBtn)
        bgV.addSubview(labelSV)
        bottomSV.addArrangedSubview(timeL)
        bottomSV.addArrangedSubview(dateL)
        labelSV.addArrangedSubview(titleL)
        labelSV.addArrangedSubview(bottomSV)
        bgV.addSubview(separatorV)
        
        let val = titleStr.components(separatedBy: ".")
        titleL.text = val[0]
        self.backgroundColor = UIColor.white
        timeL.text = dateTime
        dateL.text = timeVal

        bgV.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(self).offset(0)
            make.bottom.equalTo(self).offset(0)
//            make.height.equalTo(80)
        }
        
        recordingImage.snp.makeConstraints { (make) in
            make.left.equalTo(bgV).offset(5)
            make.centerY.equalTo(bgV)
            make.width.height.equalTo(25)
        }
        
        btnStackV.snp.makeConstraints { (make) in
            make.right.equalTo(bgV).offset(-10)
            make.centerY.equalTo(bgV)
        }
        
        shareBtn.snp.makeConstraints { (make) in
            make.height.width.equalTo(26)
        }
        
        labelSV.snp.makeConstraints { (make) in
            make.left.equalTo(recordingImage.snp.right).offset(15)
            make.right.equalTo(btnStackV.snp.left).offset(-10)
            make.centerY.equalTo(bgV)
            make.bottom.equalTo(bgV).offset(-5)
            
        }
        
        separatorV.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(bgV)
            make.height.equalTo(1)
        }
    }
}


