//
//  VoiceRecorderVC.swift
//  Call Recording
//
//  Created by TechQuadra on 09/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit
import AVFoundation

class VoiceRecorderVC: UIViewController {
    
    // To Bar View Requirements
    
    var topBarV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.navColor
        return v
    }()
    
    
    var recorderBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "mic 3"), for: .normal)
        return btn
    }()
    
    var callBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "call 1"), for: .normal)
        return btn
    }()
    
    var settingBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "setting 1"), for: .normal)
        return btn
    }()
    
    var recorderL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.text = "Voice Recorder"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 16)
        return l
    }()
    
    var separatorV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        return v
    }()

    /// Used This View While Recording
    
    var seekView: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        v.isHidden = true
        return v
    }()
    
    var seekImageV: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "line")
        image.contentMode = .scaleToFill
        return image
    }()
    
    //Static View
    
    var timeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 30)
        l.text = "00:00"
        return l
    }()
    
    var recordImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "waves dark")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    var circleImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "round")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    var recorderImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "mic 4")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    var slider: UISlider = {
        var sl = UISlider()
        sl.thumbTintColor = UIColor.init(rgb: 0xf01332)
        sl.tintColor = UIColor.init(rgb: 0xf01332)
        return sl
    }()
    
    var recordBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "record_btn"), for: .normal)
        btn.tag = 11
        return btn
    }()

    var recordingV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(rgb: 0xf01332)
        v.layer.cornerRadius = 5
        return v
    }()
    
    var listImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "recordings")
        return image
    }()
    
    var listL: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.white
        l.text = "Recordings"
        l.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        l.textAlignment = .center
        return l
    }()
    
    var listBtn: UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-delete-bin-24"), for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    var stopBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-rounded-square-24"), for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    // Edit Audio Name
    
    var bgV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor.withAlphaComponent(0.6)
        v.isHidden = true
        return v
    }()
    
    var smallPopUp: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        v.layer.cornerRadius = 5
        v.clipsToBounds = true
        return v
    }()
    
    var newNameL: UILabel = {
        let l = UILabel()
        l.text = "New Name"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        return l
    }()
    
    var nameTF: UITextFieldX = {
        let tf = UITextFieldX()
        tf.borderStyle = .none
        tf.maxLength = 15
        tf.textColor = C.theme.textColor
        return tf
    }()
    
    var bottomV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        return v
    }()
    
    var cancelBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Cancel", for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        return btn
    }()
    
    var saveBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Save", for: .normal)
        btn.setTitleColor(UIColor.red, for: .normal)
        return btn
    }()
    
    var currentPlay: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-play-30"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.isHidden = true
        return btn
    }()
    
    var statusL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Semibold", size: 15)
        l.text = "Recording..."
        l.isHidden = true
        return l
    }()
    
    var currentPause: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-pause-30"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    var currentStop: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-rounded-square-24"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()

    var currentBtnSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 40
        sv.isHidden = true
        return sv
    }()
    
    var currentEditBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "icons8-edit-24").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.isHidden = true
        btn.tintColor = C.theme.textColor
        return btn
    }()
    
    var initialTimeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        l.textAlignment = .left
        l.text = "00:00"
        return l
    }()
    
    var endTimeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 12)
        l.textAlignment = .right
        l.text = "00:00"
        return l
    }()

    var avPlayer:AVPlayer?

    
    var recording = false
    var recordings = [Recording]()
    var playTimer = Timer()
    var playing = false

    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var numberOfCards = 0
    var recordUrl: URL?
    var count = 0
    var timer = Timer()
    var tag = 0
    var isEdited = false
    var recordBtnCliked = false
    var isRecording = false

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUpView()
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        checkThisFunction()
    }
    
    func checkThisFunction() {
        getRecords()
    }
    
    func initialSetUpView()  {
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)

        view.backgroundColor = C.theme.backgroundColor


        callBtn.addTarget(self, action: #selector(callBtnClicked), for: .touchUpInside)
        settingBtn.addTarget(self, action: #selector(settingClicked), for: .touchUpInside)
        listBtn.addTarget(self, action: #selector(listBtnClicked), for: .touchUpInside)
        recordBtn.addTarget(self, action: #selector(recordBtnAction), for: .touchUpInside)
        stopBtn.addTarget(self, action: #selector(stopAudioRecorder), for: .touchUpInside)
        saveBtn.addTarget(self, action: #selector(saveButtonClicked), for: .touchUpInside)
        deleteBtn.addTarget(self, action: #selector(deleteAudioFile), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
        currentPlay.addTarget(self, action: #selector(playBtnAction), for: .touchUpInside)
        currentPause.addTarget(self, action: #selector(playBtnAction), for: .touchUpInside)
        currentStop.addTarget(self, action: #selector(stopRecorderAction), for: .touchUpInside)
        currentEditBtn.addTarget(self, action: #selector(editAudiofile), for: .touchUpInside)
    }
    
    
    func getRecords()  {
        self.recordings.removeAll()
        
          let fileManager = FileManager.default
          let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
          let directoryContents = try! fileManager.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil)
          for audio in directoryContents {
            let recording = Recording(fileURL: audio, createdAt: CommonMethods.getCreationDate(for: audio))
            recordings.append(recording)
          }
        recordings.sort(by: { $0.createdAt.compare($1.createdAt) == .orderedDescending})
              if recordings.count > 0 {
               self.statusL.isHidden = false
                let val = recordings[0].fileURL.lastPathComponent.components(separatedBy: ".")
               self.statusL.text = val[0]
               self.nameTF.text = val[0]
               self.tag = 0
               self.currentPlay.isHidden = false
               self.currentEditBtn.isHidden = false
                self.initialTimeL.isHidden = false
                self.endTimeL.isHidden = false
           } else {
              self.currentPlay.isHidden = true
              self.currentEditBtn.isHidden = true
              self.statusL.isHidden = true
                self.initialTimeL.isHidden = true
                self.endTimeL.isHidden = true

          }
    }
    
    
    func setUpViews()  {
        view.addSubview(topBarV)
        topBarV.addSubview(callBtn)
        topBarV.addSubview(settingBtn)
        topBarV.addSubview(recorderBtn)
        topBarV.addSubview(recorderL)
        view.addSubview(separatorV)
        view.addSubview(timeL)
        view.addSubview(recordImage)
        view.addSubview(circleImage)
        view.addSubview(recorderImage)
        view.addSubview(slider)
        view.addSubview(recordBtn)
        view.addSubview(deleteBtn)
        view.addSubview(stopBtn)
        view.addSubview(recordingV)
        recordingV.addSubview(listImage)
        recordingV.addSubview(listL)
        recordingV.addSubview(listBtn)
        view.addSubview(currentPlay)
        view.addSubview(currentBtnSV)
        currentBtnSV.addArrangedSubview(currentPause)
        currentBtnSV.addArrangedSubview(currentStop)
        view.addSubview(initialTimeL)
        view.addSubview(endTimeL)
        view.addSubview(currentEditBtn)
        
        view.addSubview(bgV)
        bgV.addSubview(smallPopUp)
        smallPopUp.addSubview(newNameL)
        smallPopUp.addSubview(nameTF)
        smallPopUp.addSubview(bottomV)
        smallPopUp.addSubview(saveBtn)
        smallPopUp.addSubview(cancelBtn)
        
        view.addSubview(seekView)
        seekView.addSubview(seekImageV)
        view.addSubview(statusL)
        
        statusL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(currentPlay.snp.bottom).offset(15)
        }
        
        currentEditBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(statusL)
            make.left.equalTo(statusL.snp.right).offset(5)
            make.height.width.equalTo(25)
        }
//        slider.isHidden = true
        
        seekView.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.left.right.equalTo(slider)
            make.centerY.equalTo(slider)
        }
        
        seekImageV.snp.makeConstraints { (make) in
            make.centerY.equalTo(seekView)
            make.left.equalTo(seekView)
            make.right.equalTo(seekView)
            make.height.equalTo(8)
        }
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(160)
            } else {
                make.height.equalTo(130)
            }
        }
        
        recorderBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(topBarV).offset(-10)
            make.centerX.equalTo(view)
            make.height.width.equalTo(50)
        }
        
        recorderL.snp.makeConstraints { (make) in
            make.centerX.equalTo(recorderBtn)
            make.top.equalTo(recorderBtn.snp.bottom).offset(8)
        }
        
        callBtn.snp.makeConstraints { (make) in
            make.left.equalTo(topBarV).offset(25)
            make.centerY.equalTo(topBarV).offset(-10)
            make.height.width.equalTo(20)
        }
        
        settingBtn.snp.makeConstraints { (make) in
            make.right.equalTo(topBarV).offset(-25)
            make.height.width.equalTo(callBtn)
            make.centerY.equalTo(topBarV).offset(-10)
        }
        
        separatorV.snp.makeConstraints { (make) in
            make.top.equalTo(topBarV.snp.bottom)
            make.left.right.equalTo(topBarV)
            make.height.equalTo(1)
        }
        
        timeL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(separatorV.snp.bottom).offset(30)
        }
        
        recordImage.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(30)
            make.right.equalTo(view).offset(-30)
            make.top.equalTo(timeL.snp.bottom).offset(80)
            make.height.equalTo(30)
        }
        
        circleImage.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(recordImage)
            make.width.height.equalTo(80)
        }
        
        recorderImage.snp.makeConstraints { (make) in
            make.centerY.centerX.equalTo(circleImage)
            make.height.width.equalTo(30)
        }
        
        currentPlay.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(circleImage.snp.bottom).offset(45)
            make.height.width.equalTo(30)
        }
        
        currentBtnSV.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(circleImage.snp.bottom).offset(45)
            make.height.equalTo(30)
        }
        
        currentPause.snp.makeConstraints { (make) in
            make.width.equalTo(30)
        }
        
        initialTimeL.snp.makeConstraints { (make) in
            make.left.equalTo(slider)
            make.bottom.equalTo(slider.snp.top).offset(-4)
        }
        
        endTimeL.snp.makeConstraints { (make) in
            make.right.equalTo(slider)
            make.bottom.equalTo(slider.snp.top).offset(-4)
        }

        
        slider.snp.makeConstraints { (make) in
            make.left.right.equalTo(recordImage)
            make.top.equalTo(currentPlay.snp.bottom).offset(50)
        }
        
        recordBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(slider.snp.bottom).offset(14)
            make.height.width.equalTo(70)
        }
        
        deleteBtn.snp.makeConstraints { (make) in
            make.right.equalTo(recordBtn.snp.left).offset(-50)
            make.centerY.equalTo(recordBtn)
            make.height.width.equalTo(30)
        }
        
        stopBtn.snp.makeConstraints { (make) in
            make.left.equalTo(recordBtn.snp.right).offset(50)
            make.centerY.equalTo(recordBtn)
            make.height.width.equalTo(30)
        }
        
        recordingV.snp.makeConstraints { (make) in
            make.bottom.equalTo(view).offset(-30)
            make.centerX.equalTo(view)
            make.height.equalTo(40)
            make.width.equalTo(130)
        }
        
        listImage.snp.makeConstraints { (make) in
            make.centerY.equalTo(recordingV)
            make.height.width.equalTo(20)
            make.left.equalTo(recordingV).offset(10)
        }
        
        listL.snp.makeConstraints { (make) in
            make.left.equalTo(listImage.snp.right).offset(8)
            make.right.equalTo(recordingV).offset(-5)
            make.centerY.equalTo(recordingV)
        }
        
        listBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(recordingV)
        }
        
        bgV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        smallPopUp.snp.makeConstraints { (make) in
            make.centerY.equalTo(view)
            make.left.equalTo(bgV).offset(20)
            make.right.equalTo(bgV).offset(-20)
        }
        
        newNameL.snp.makeConstraints { (make) in
            make.left.top.equalTo(smallPopUp).offset(20)
            make.right.equalTo(smallPopUp).offset(-20)
        }
        
        nameTF.snp.makeConstraints { (make) in
            make.left.right.equalTo(newNameL)
            make.height.equalTo(40)
            make.top.equalTo(newNameL.snp.bottom).offset(10)
        }
        
        bottomV.snp.makeConstraints { (make) in
            make.left.right.equalTo(nameTF)
            make.top.equalTo(nameTF.snp.bottom).offset(2)
            make.height.equalTo(1)
        }
        
        saveBtn.snp.makeConstraints { (make) in
            make.right.equalTo(nameTF)
            make.top.equalTo(bottomV.snp.bottom).offset(30)
            make.width.equalTo(100)
            make.bottom.equalTo(smallPopUp.snp.bottom).offset(-25)
        }
        
        cancelBtn.snp.makeConstraints { (make) in
            make.right.equalTo(saveBtn.snp.left).offset(-10)
            make.top.bottom.width.equalTo(saveBtn)
        }
    }
    
    @objc func settingClicked() {
        let VC = SettingsVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func callBtnClicked() {
        let VC = CallRecorderVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func listBtnClicked() {
        let VC = VoiceRecordingListVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func premiumBtnClicked() {
        let VC = UpgradeVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.view.backgroundColor = C.theme.backgroundColor
        self.topBarV.backgroundColor = Application.Colors.mattLight
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.view.backgroundColor = .white
        self.topBarV.backgroundColor = UIColor.white
    }

    
    // Delete last Audio when it is not in use
    func deleteAfterGo()  {
        do {
             let fileManager = FileManager.default
            if fileManager.fileExists(atPath: audioRecorder.url.path) {
                try fileManager.removeItem(atPath: audioRecorder.url.path)
            } else {
                print("File does not exist")
            }
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
}

//MARK: UPDATE AUDIO NAME
extension VoiceRecorderVC {
    
    @objc fileprivate func editRecordedAudioName() {
//        if nameTF.text == recordings[tag].fileURL.lastPathComponent {
//            self.bgV.isHidden = true
//            return
//        }
        
        var urlStr: URL = self.recordings[tag].fileURL
        urlStr.deleteLastPathComponent()
        urlStr.appendPathComponent(nameTF.text! + ".m4a")
        do {
            try FileManager.default.moveItem(atPath: self.recordings[tag].fileURL.path, toPath: urlStr.path)
            self.cancelBtnClicked()
            self.getRecords()
        } catch {
            print(error.localizedDescription)
            Application.showAlertWithActions(title: error.localizedDescription, subTitle: "", message: "", actionTitle: "Ok", completion: { (alert) in
                self.present(alert, animated: true, completion: nil)
            }) { (firstActiom) in
                self.cancelBtnClicked()
            }
        }
        print(urlStr)
    }
    
    fileprivate func EditCurrentRecordedAudio() {
//        if nameTF.text == recordUrl?.lastPathComponent {
//            checkThisFunction()
//            self.bgV.isHidden = true
//            return
//        }
        let oldUrl = self.recordUrl
        self.recordUrl?.deleteLastPathComponent()
        self.recordUrl?.appendPathComponent(nameTF.text! + ".m4a" )
        do {
            checkThisFunction()
            try FileManager.default.moveItem(atPath: oldUrl!.path, toPath: self.recordUrl!.path)
            self.cancelBtnClicked()
            self.getRecords()
        } catch {
            print(error.localizedDescription)
            Application.showAlertWithActions(title: error.localizedDescription, subTitle: "", message: "", actionTitle: "Ok", completion: { (alert) in
                self.present(alert, animated: true, completion: nil)
            }) { (firstActiom) in
                self.bgV.isHidden = true
            }
        }
    }
    
    @objc func saveButtonClicked() {
        if self.isEdited {
            editRecordedAudioName()
            self.isEdited = false
        } else {
            EditCurrentRecordedAudio()
        }
        self.getRecords()
    }
}

//MARK: Recorded audio functionality
extension VoiceRecorderVC {
    @objc func deleteAudioFile() {
        do {
             let fileManager = FileManager.default
             if fileManager.fileExists(atPath: audioRecorder.url.path) {
             try fileManager.removeItem(atPath: audioRecorder.url.path)
                self.recordBtn.setImage(#imageLiteral(resourceName: "record_btn"), for: .normal)
                self.deleteBtn.isHidden = true
                self.stopBtn.isHidden = true
                self.statusL.text = "Recording..."
                self.slider.isHidden = false
                self.seekView.isHidden = true
                self.currentEditBtn.isHidden = false
                audioRecorder = nil
                self.recording = false
                self.timer.invalidate()
                self.timeL.text = "00:00"
                self.getRecords()
            } else {
                print("File does not exist")
            }
          }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    @objc func cancelBtnClicked() {
        self.bgV.isHidden = true
    }
    
    // UPDATE TIMER LABEL VALUE

    @objc func updateTimerValue() {
        count = count + 1
        CommonMethods.hmsFrom(seconds: count) { hours, minutes, seconds in

            let hoursStr = CommonMethods.getStringFrom(seconds: hours)
            let minutes = CommonMethods.getStringFrom(seconds: minutes)
            let seconds = CommonMethods.getStringFrom(seconds: seconds)
            if hours <= 0 {
                self.timeL.text = "\(minutes):\(seconds)"
            } else {
                self.timeL.text = "\(hoursStr):\(minutes):\(seconds)"
            }
        }
    }

}

//MARK: INITIALIZE RECORDER SETUP
extension VoiceRecorderVC {
    
    // set initialStep for audio Recorder
    
    func setInitialAudioRecorder() {
        if let number: Int = UserDefaults.standard.object(forKey: "myNumbers") as? Int {
            self.numberOfCards = number
        }
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            if hasPermission {
                
            }
        }
    }

     
     func startAudioSession() {
         recordingSession = AVAudioSession.sharedInstance()
         
         do {
             try recordingSession.setCategory(AVAudioSession.Category.playAndRecord)
             try recordingSession.setActive(true)
             recordingSession.requestRecordPermission() { [unowned self] allowed in
                 DispatchQueue.main.async {
                     if allowed {
                         self.setupRecorder()
                     } else {
                     }
                 }
             }
         } catch {
         }
     }

     func getCacheDirectory() -> URL {
         let fm = FileManager.default
         let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
         return docsurl
     }
     
     func getFileURL() -> URL{
         let path  = getCacheDirectory()
         let filePath = path.appendingPathComponent("Record-\(numberOfCards)")
         return filePath
     }

    func setupRecorder() {
         let recordSettings = [AVFormatIDKey : kAudioFormatAppleLossless,
                               AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
                               AVEncoderBitRateKey : 320000,
                               AVNumberOfChannelsKey : 2,
                               AVSampleRateKey : 44100.0 ] as [String : Any]
         
         do {
             audioRecorder = try AVAudioRecorder(url: getFileURL(), settings: recordSettings)
             audioRecorder.delegate = self
             audioRecorder.prepareToRecord()
            if recordBtnCliked == true {
                self.recordFinal()
            }
         }
         catch {
             print("\(error)")
         }
         
     }

}


//MARK: CURRENT AUDIO PLAYER
extension VoiceRecorderVC {
    // Get Current Play
    
    func cacheData() -> URL {
        let fm = FileManager.default
        let docs = try! fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return docs
    }
    
    func getFilePath() -> URL {
        let path = cacheData()
        let filePath = path.appendingPathComponent("\(String(describing: recordings[tag].fileURL.lastPathComponent))")
        return filePath
    }
    
    // PLAY CURRENT RECORDER OR LAST RECORDED AUDIO
    
    @objc func playBtnAction() {
        if audioPlayer == nil {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: getFilePath())
                self.endTimeL.text = audioPlayer.duration.stringFromTimeInterval()
                slider.maximumValue = Float(audioPlayer.duration)
                currentPlay.isHidden = true
                currentBtnSV.isHidden = false
                audioPlayer.delegate = self
                audioPlayer.prepareToPlay()
                audioPlayer.volume = 10.0
                audioPlayer.play()
                playTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSliderVal), userInfo: nil, repeats: true)
            } catch {
                print("audio file error")
            }
            avPlayer?.play()
        } else {
            if audioPlayer.isPlaying {
                currentPlay.isHidden = false
                currentBtnSV.isHidden = true
                audioPlayer.pause()
                timer.invalidate()
            } else {
                currentPlay.isHidden = true
                currentBtnSV.isHidden = false
                audioPlayer.play()
                playTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSliderVal), userInfo: nil, repeats: true)
            }
        }
    }
    
    // STOP RECORDER OF CURRENT AUDIO PLAYER
    
    @objc func stopRecorderAction() {
        playTimer.invalidate()
        audioPlayer.stop()
        audioPlayer = nil
        self.slider.value = 0
        self.timeL.text = "00:00"
        currentBtnSV.isHidden = true
        currentPlay.isHidden = false
    }
    
    // UPDATE SLIDER VALUE AND TIMER LABLE VALUE
    
    @objc func updateSliderVal()  {
        if audioPlayer != nil {
            slider.value = Float(audioPlayer.currentTime)
            self.timeL.text = audioPlayer.currentTime.stringFromTimeInterval()
        }
    }
    
    // UPDATE CURRENT AUDIO FILE NAME
    
    @objc func editAudiofile() {
        self.isEdited = true
        let val = self.recordings[tag].fileURL.lastPathComponent.components(separatedBy: ".")
        self.nameTF.text = val[0]
        let trans = CGAffineTransform(translationX: view.frame.width, y: 0)
        self.bgV.transform = trans
        UIView.animate(withDuration: 1.0, animations: {
            self.bgV.isHidden = false
            self.bgV.transform = .identity
        }) { (true) in
        }
    }

    
    @objc func recordBtnAction() {
        recordBtnCliked = true
        setInitialAudioRecorder()
        startAudioSession()
    }
    
    func recordFinal()  {
        if recording == false {
            audioRecorder.record()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerValue), userInfo: nil, repeats: true)
            self.recordBtn.setImage(#imageLiteral(resourceName: "pause 1"), for: .normal)
            self.deleteBtn.isHidden = false
            self.stopBtn.isHidden = false
            numberOfCards = numberOfCards + 1
            self.statusL.text = "Recording..."
            self.statusL.isHidden = false
            self.slider.isHidden = true
            self.seekView.isHidden = false
            self.currentPlay.isHidden = false
            self.initialTimeL.isHidden = true
            self.endTimeL.isHidden = true
            self.isEdited = false
            self.recordBtnCliked = false
            recording = true
            isRecording = true
        }
        else {
            if isRecording {
                self.statusL.text = "Paused"
                self.recordBtn.setImage(#imageLiteral(resourceName: "play 1"), for: .normal)
                audioRecorder.pause()
                timer.invalidate()
                self.isRecording = false
            } else {
                self.statusL.text = "Recording..."
                self.recordBtn.setImage(#imageLiteral(resourceName: "pause 1"), for: .normal)
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerValue), userInfo: nil, repeats: true)
                self.isRecording = true
                audioRecorder.record()
            }
        }
    }
    
    @objc func stopAudioRecorder() {
        audioRecorder.stop()
        self.recordBtn.setImage(#imageLiteral(resourceName: "record_btn"), for: .normal)
        self.deleteBtn.isHidden = true
        self.stopBtn.isHidden = true
        self.recordUrl = audioRecorder.url
        self.nameTF.text = recordUrl?.lastPathComponent
        self.statusL.text = "Recording..."
        self.slider.isHidden = false
        self.seekView.isHidden = true
        self.statusL.text = nameTF.text
        self.currentEditBtn.isHidden = false
        self.isRecording = false
        audioRecorder = nil
        self.recording = false
        let trans = CGAffineTransform(translationX: view.frame.width, y: 0)
           self.bgV.transform = trans
           UIView.animate(withDuration: 1.0, animations: {
               self.bgV.isHidden = false
               self.bgV.transform = .identity
           }) { (true) in
        }
        self.count = 0
        timer.invalidate()
        self.timeL.text = "00:00"
        audioRecorder = nil
        UserDefaults.standard.set(numberOfCards, forKey: "myNumbers")
    }

    @objc func editBtnClicked() {
        let trans = CGAffineTransform(translationX: view.frame.width, y: 0)
           self.bgV.transform = trans
           UIView.animate(withDuration: 1.0, animations: {
               self.bgV.isHidden = false
               self.bgV.transform = .identity
           }) { (true) in
        }

    }
}


extension VoiceRecorderVC: AVAudioRecorderDelegate, AVAudioPlayerDelegate  {
    //MARK: Delegates
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        slider.value = 0
        playTimer.invalidate()
        self.currentBtnSV.isHidden = true
        self.currentPlay.isHidden = false
        self.initialTimeL.isHidden = false
        self.endTimeL.isHidden = false
        self.audioPlayer.stop()
        self.audioPlayer = nil
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }

}

