//
//  CallRecorderVC.swift
//  Call Recording
//
//  Created by TechQuadra on 09/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class CallRecorderVC: UIViewController {

    var topBarV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.navColor
        return v
    }()
    
    var callRecordBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "call 2"), for: .normal)
        return btn
    }()
    
    var audioBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "mic 1"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    var settingBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "setting 1"), for: .normal)
        return btn
    }()
    
    var recorderL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.text = "Call Recorder"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 16)
        return l
    }()
    
    var separatorV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        return v
    }()
    
    var timeL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-SemiBold", size: 30)
        l.text = "00:00"
        return l
    }()
    
    var recordImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "waves dark")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    var circleImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "round")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    var recorderImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "recorder")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    var slider: UISlider = {
        var sl = UISlider()
        sl.thumbTintColor = UIColor.init(rgb: 0xf01332)
        return sl
    }()
    
    var recordBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "record_btn"), for: .normal)
        return btn
    }()

    var recordingV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(rgb: 0xf01332)
        v.layer.cornerRadius = 5
        return v
    }()
    
    var listImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "recordings")
        return image
    }()
    
    var listL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.text = "Recordings"
        l.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        l.textAlignment = .center
        return l
    }()
    
    var listBtn: UIButton = {
        let btn = UIButton()
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUpView()
        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUpView()  {
//        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)

        view.backgroundColor = C.theme.backgroundColor
        listBtn.addTarget(self, action: #selector(listBtnClicked), for: .touchUpInside)
        settingBtn.addTarget(self, action: #selector(goToSettingVC), for: .touchUpInside)
        audioBtn.addTarget(self, action: #selector(gotoaudioRecordingVC), for: .touchUpInside)
    }
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.view.backgroundColor = C.theme.backgroundColor
        self.topBarV.backgroundColor = Application.Colors.mattLight
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.view.backgroundColor = .white
        self.topBarV.backgroundColor = UIColor.white
    }

    
    func setUpViews()  {
        view.addSubview(topBarV)
        topBarV.addSubview(audioBtn)
        topBarV.addSubview(settingBtn)
        topBarV.addSubview(callRecordBtn)
        topBarV.addSubview(recorderL)
        view.addSubview(separatorV)
        view.addSubview(timeL)
        view.addSubview(recordImage)
        view.addSubview(circleImage)
        view.addSubview(recorderImage)
        view.addSubview(slider)
        view.addSubview(recordBtn)
        view.addSubview(recordingV)
        recordingV.addSubview(listImage)
        recordingV.addSubview(listL)
        recordingV.addSubview(listBtn)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(160)
            } else {
                make.height.equalTo(130)
            }
        }
        
        callRecordBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(topBarV).offset(-10)
            make.centerX.equalTo(view)
            make.height.width.equalTo(50)
        }
        
        recorderL.snp.makeConstraints { (make) in
            make.centerX.equalTo(callRecordBtn)
            make.top.equalTo(callRecordBtn.snp.bottom).offset(8)
        }
        
        audioBtn.snp.makeConstraints { (make) in
            make.left.equalTo(topBarV).offset(25)
            make.centerY.equalTo(topBarV).offset(-10)
            make.height.width.equalTo(20)
        }
        
        settingBtn.snp.makeConstraints { (make) in
            make.right.equalTo(topBarV).offset(-25)
            make.height.width.equalTo(audioBtn)
            make.centerY.equalTo(topBarV).offset(-10)
        }
        
        separatorV.snp.makeConstraints { (make) in
            make.top.equalTo(topBarV.snp.bottom)
            make.left.right.equalTo(topBarV)
            make.height.equalTo(1)
        }
        
        timeL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(separatorV.snp.bottom).offset(30)
        }
        
        recordImage.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(30)
            make.right.equalTo(view).offset(-30)
            make.centerY.equalTo(view).offset(-15)
            make.height.equalTo(50)
        }
        
        circleImage.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(recordImage)
            make.width.height.equalTo(90)
        }
        
        recorderImage.snp.makeConstraints { (make) in
            make.centerY.centerX.equalTo(circleImage)
            make.height.width.equalTo(40)
        }
        
        slider.snp.makeConstraints { (make) in
            make.left.right.equalTo(recordImage)
            make.top.equalTo(recordImage.snp.bottom).offset(80)
        }
        
        recordBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(slider.snp.bottom).offset(14)
            make.height.width.equalTo(70)
        }
        
        recordingV.snp.makeConstraints { (make) in
            make.bottom.equalTo(view).offset(-30)
            make.centerX.equalTo(view)
            make.height.equalTo(40)
            make.width.equalTo(130)
        }
        
        listImage.snp.makeConstraints { (make) in
            make.centerY.equalTo(recordingV)
            make.height.width.equalTo(20)
            make.left.equalTo(recordingV).offset(10)
        }
        
        listL.snp.makeConstraints { (make) in
            make.left.equalTo(listImage.snp.right).offset(8)
            make.right.equalTo(recordingV).offset(-5)
            make.centerY.equalTo(recordingV)
        }
        
        listBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(recordingV)
        }
        
    }

    @objc func listBtnClicked() {
        let vc = CallRecordingListVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func gotoaudioRecordingVC() {
        let VC = VoiceRecorderVC()
        Application.Stored.isBackToAudio = true
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @objc func goToSettingVC() {
        let VC = SettingsVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
