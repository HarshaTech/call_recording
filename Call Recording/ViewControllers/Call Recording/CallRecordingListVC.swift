//
//  CallRecordingListVC.swift
//  Call Recording
//
//  Created by TechQuadra on 09/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class CallRecordingListVC: UIViewController {
    
    var topBarV = CustomNavigationBar(_isBack: true)
    
    let tableV: UITableView = {
        let table = UITableView()
        table.backgroundColor = C.theme.backgroundColor
        table.separatorStyle = .none
        table.tableFooterView = UIView()
        table.showsVerticalScrollIndicator = false
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUpV()
        setUpViews()
    }
    
    func setUpViews()  {
        view.addSubview(topBarV)
        view.addSubview(tableV)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(90)
            } else {
                make.height.equalTo(70)
            }
        }
        
        tableV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(10)
            make.right.equalTo(view).offset(-10)
            make.top.equalTo(topBarV.snp.bottom).offset(10)
            make.bottom.equalTo(view).offset(-10)
        }
    }
    
    func initialSetUpV()  {
        self.view.backgroundColor = C.theme.backgroundColor
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        topBarV.backButton.addTarget(self, action: #selector(backBtnClicked), for: .touchUpInside)
        topBarV.titleL.text = "Call Recordings"
        tableV.register(CallRecordingListTVV.self, forCellReuseIdentifier: CallRecordingListTVV.identifier)
        tableV.delegate = self
        tableV.dataSource = self
    }
    
    @objc func backBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CallRecordingListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableV.dequeueReusableCell(withIdentifier: CallRecordingListTVV.identifier) as! CallRecordingListTVV
        cell.populate()
        return cell
    }
    
}


class CallRecordingListTVV: UITableViewCell {
    
    var bgV: UIView = {
        let v = UIView()
        v.backgroundColor = C.theme.backgroundColor
        return v
    }()
    
    var recordingImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "call 3")
        image.clipsToBounds = true
        return image
    }()
    
    var shareBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        return btn
    }()
    
    var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        return btn
    }()

    var editBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
        return btn
    }()
    
    var btnStackV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 7
        return sv
    }()
    
    var titleL: UILabel = {
        let l = UILabel()
        l.text = "Audio"
        l.font = UIFont(name: "Montserrat-Regular", size: 18)
        l.textColor = C.theme.textColor
        return l
    }()

    var timeL: UILabel = {
        let l = UILabel()
        l.text = "04:30"
        l.font = UIFont(name: "Montserrat-Regular", size: 13)
        l.textColor = C.theme.darkGrayTextColor
        return l
    }()

    var dateL: UILabel = {
        let l = UILabel()
        l.text = "22/05/2020"
        l.font = UIFont(name: "Montserrat-Regular", size: 13)
        l.textColor = C.theme.darkGrayTextColor
        return l
    }()
    
    var bottomSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillProportionally
        sv.spacing = 10
        return sv
    }()
    
    var labelSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 2
        sv.distribution = .fillProportionally
        return sv
    }()

    var separatorV: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.gray
        return v
        
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate() {
        self.backgroundColor = C.theme.backgroundColor
        self.addSubview(bgV)
        bgV.addSubview(recordingImage)
        bgV.addSubview(btnStackV)
        btnStackV.addArrangedSubview(editBtn)
        btnStackV.addArrangedSubview(shareBtn)
        btnStackV.addArrangedSubview(deleteBtn)
        bgV.addSubview(labelSV)
        bottomSV.addArrangedSubview(timeL)
        bottomSV.addArrangedSubview(dateL)
        labelSV.addArrangedSubview(titleL)
        labelSV.addArrangedSubview(bottomSV)
        bgV.addSubview(separatorV)
        
        bgV.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-0)
//            make.height.equalTo(80)
        }
        
        recordingImage.snp.makeConstraints { (make) in
            make.left.equalTo(bgV).offset(5)
            make.centerY.equalTo(bgV)
            make.width.height.equalTo(25)
        }
        
        btnStackV.snp.makeConstraints { (make) in
            make.right.equalTo(bgV).offset(-10)
            make.centerY.equalTo(bgV)
        }
        
        shareBtn.snp.makeConstraints { (make) in
            make.height.width.equalTo(26)
        }
        
        labelSV.snp.makeConstraints { (make) in
            make.left.equalTo(recordingImage.snp.right).offset(15)
            make.right.equalTo(btnStackV.snp.left).offset(-10)
            make.centerY.equalTo(bgV)
            make.bottom.equalTo(bgV).offset(-5)
            
        }
        
        separatorV.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(bgV)
            make.height.equalTo(1)
        }
    }
}
