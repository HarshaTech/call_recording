//
//  HelpAndSupportVC.swift
//  Call Recording
//
//  Created by TechQuadra on 08/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class HelpAndSupportVC: UIViewController {
    
    var faqV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "faq - light"),  title: "FAQ")
    var contactV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "contact us - light"),  title: "Contact Us")
    var termsV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "terms of use - light"),  title: "Terms of Use")
    var privacyV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "privacy policy - light"),  title: "Privacy Policy")
    var phoneLaw = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "ph rec. laws - light"),  title: "Phone Recording Law")
    var lawV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "ph rec. laws - light"),  title: "Phone Recording Law")
    var topBarV = CustomNavigationBar(_isBack: true)

    var stackV: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 25
        sv.distribution = .fillEqually
        sv.axis = .vertical
        return sv
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setUpViews()
    }
    
    func initialSetUp()  {
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)
        let terms = UITapGestureRecognizer(target: self, action: #selector(termsClicked))
        self.termsV.addGestureRecognizer(terms)
        let privacy = UITapGestureRecognizer(target: self, action: #selector(privacyClicked))
        self.privacyV.addGestureRecognizer(privacy)

        self.view.backgroundColor = C.theme.backgroundColor
        topBarV.titleL.text = "Help And Support"
        topBarV.backButton.addTarget(self, action: #selector(backBtnClicked), for: .touchUpInside)
    }
    
    func setUpViews()  {
        view.addSubview(topBarV)
        view.addSubview(stackV)
//        stackV.addArrangedSubview(faqV)
//        stackV.addArrangedSubview(contactV)
        stackV.addArrangedSubview(termsV)
        stackV.addArrangedSubview(privacyV)
        stackV.addArrangedSubview(phoneLaw)
        
        topBarV.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(90)
            } else {
                make.height.equalTo(70)
            }
        }
        
        stackV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.top.equalTo(topBarV.snp.bottom).offset(30)
        }
        
        termsV.snp.makeConstraints { (make) in
            make.height.equalTo(45)
        }
    }
    
    @objc func backBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.view.backgroundColor = C.theme.backgroundColor
        self.lawV.backgroundColor = Application.Colors.mattLight

    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.view.backgroundColor = .white
        self.lawV.backgroundColor = C.theme.navColor

    }

    @objc func termsClicked() {
        let VC = TermsAndconditionVC()
        VC.titleStr = "Terms And Condition"
        VC.urlStr = Application.API.termsAndCondUrl
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func privacyClicked() {
        let VC = TermsAndconditionVC()
        VC.titleStr = "Privacy Policy"
        VC.urlStr = Application.API.privacyUrl
        self.navigationController?.pushViewController(VC, animated: true)

    }
}
