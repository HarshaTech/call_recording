//
//  PrivacyViewController.swift
//  Call Recording
//
//  Created by TechQuadra on 08/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController {

    let topImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "start trial")
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        return image
    }()
    
    let textL: UILabel = {
        let  l = UILabel()
        l.textAlignment = .center
        l.numberOfLines = 0
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Regular", size: 15)
        return l
    }()
    
    let headingL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.font = UIFont(name: "Montserrat-SemiBold", size: 22)
        l.text = "Privacy Notice"
        l.textColor = C.theme.textColor
//        l.backgroundColor = UIColor.red
        return l
    }()
    
    var continueBtn: UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(#imageLiteral(resourceName: "recordings btn"), for: .normal)
        btn.setTitle("Continue", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 20)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp()  {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = C.theme.backgroundColor
        textL.text = "Your privacy is our conern. We intend to provide transparency and ensure user privacy. Please learn more about how we process the data that can be obtained about you or your device. By continuing you confirm that you acknowledge and accept our Privacy Policy"
        self.continueBtn.addTarget(self, action: #selector(gotoAccessView), for: .touchUpInside)
    }
    
    func setUpViews()  {
        view.addSubview(topImage)
        view.addSubview(headingL)
        view.addSubview(textL)
        view.addSubview(continueBtn)
        
        topImage.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(400)
        }
        
        headingL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(topImage.snp.bottom)
        }
        
        textL.snp.makeConstraints { (make) in
            make.top.equalTo(headingL.snp.bottom).offset(10)
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
        }
        
        continueBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(textL.snp.bottom).offset(30)
            make.width.equalTo(250)
            make.height.equalTo(45)
        }
    }
    
    @objc func gotoAccessView() {
        let VC = AccessFeatureVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
