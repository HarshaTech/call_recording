//
//  SettingsVC.swift
//  Call Recording
//
//  Created by TechQuadra on 03/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    var customNavigationBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()

    var settingIcon: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "setting 2")
        image.clipsToBounds = true
        return image
    }()
    
    var callBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "call 1"), for: .normal)
        return btn
    }()
    
    var audioBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "mic 1"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    var setttingsL: UILabel = {
        let l = UILabel()
        l.text = "Settings"
        l.textColor = C.theme.textColor
        l.font = UIFont(name: "Montserrat-Medium", size: 18)
        l.textAlignment = .center
        return l
    }()
    
    var bgImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "waves dark")
        image.clipsToBounds = true
        return image
    }()
    
    var logoImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "round")
        image.clipsToBounds = true
        return image
    }()
    
    var centerIcon: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "setting 3")
        image.clipsToBounds = true
        return image
    }()
    
    var stackV: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 10
        sv.distribution = .fillEqually
        sv.axis = .vertical
        return sv
    }()
    
    var transcriptionView = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "transcription lang"), title: "Transcription Language")
    var darkModeV = SettingViews(_isArrow: false , _isSwitch: true, image: #imageLiteral(resourceName: "modes"),  title: "Dark Mode")
    var helpSupprtV = SettingViews(_isArrow: true, _isSwitch: false, image: #imageLiteral(resourceName: "help & support"),  title: "Help & Support")
    var shareApp = SettingViews(_isArrow: false, _isSwitch: false, image: #imageLiteral(resourceName: "share this app"),  title: "Share This App")
    var restorePurchase = SettingViews(_isArrow: false, _isSwitch: false, image: #imageLiteral(resourceName: "restore purchases") ,title: "Restore Purchase")


    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUpViews()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUpViews() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(_:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(_:)), name: .darkModeDisabled, object: nil)
        if C.theme.theme == Theme.light {
            NotificationCenter.default.post(name: .darkModeDisabled, object: nil)
        } else {
            NotificationCenter.default.post(name: .darkModeEnabled, object: nil)
        }

        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        view.backgroundColor = UIColor.white
        transcriptionView.rightL.text = "English(India)"
        let helpGesture = UITapGestureRecognizer(target: self, action: #selector(helpBtnClicked))
        helpSupprtV.addGestureRecognizer(helpGesture)
        
        let languageGesture = UITapGestureRecognizer(target: self, action: #selector(languageClicked))
        transcriptionView.addGestureRecognizer(languageGesture)
        let themeGesture = UITapGestureRecognizer(target: self, action: #selector(darkModeSwitched(_:)))
        darkModeV.addGestureRecognizer(themeGesture)
 
        audioBtn.addTarget(self, action: #selector(audioBtnClicked), for: .touchUpInside)
        callBtn.addTarget(self, action: #selector(callBtnClicked), for: .touchUpInside)
        
    }
    
    func setupViews() {
        view.addSubview(customNavigationBarView)
        customNavigationBarView.addSubview(settingIcon)
        customNavigationBarView.addSubview(setttingsL)
        customNavigationBarView.addSubview(callBtn)
        customNavigationBarView.addSubview(audioBtn)
        view.addSubview(bgImage)
        view.addSubview(logoImage)
        view.addSubview(centerIcon)
        view.addSubview(stackV)
        stackV.addArrangedSubview(transcriptionView)
        stackV.addArrangedSubview(darkModeV)
        stackV.addArrangedSubview(helpSupprtV)
        stackV.addArrangedSubview(shareApp)
        stackV.addArrangedSubview(restorePurchase)
        
        customNavigationBarView.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(view)
            if DeviceType.hasNotch {
                make.height.equalTo(160)
            } else {
                make.height.equalTo(140)
            }
        }
        
        settingIcon.snp.makeConstraints { (make) in
            make.centerX.equalTo(customNavigationBarView)
            make.height.width.equalTo(70)
             if DeviceType.hasNotch {
                make.top.equalTo(customNavigationBarView).offset(35)

             } else {
                make.top.equalTo(customNavigationBarView).offset(20)

            }
        }
        
        callBtn.snp.makeConstraints { (make) in
            make.left.equalTo(customNavigationBarView).offset(40)
            make.centerY.equalTo(settingIcon)
            make.height.width.equalTo(25)
        }
        
        audioBtn.snp.makeConstraints { (make) in
            make.right.equalTo(customNavigationBarView).offset(-40)
            make.height.width.equalTo(callBtn)
            make.centerY.equalTo(settingIcon)
        }
        
        setttingsL.snp.makeConstraints { (make) in
            make.centerX.equalTo(customNavigationBarView)
            make.top.equalTo(settingIcon.snp.bottom).offset(15)
        }
        
        bgImage.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(customNavigationBarView.snp.bottom).offset(15)
            make.height.equalTo(110)
            make.width.equalTo(240)
        }
        
        logoImage.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.centerY.equalTo(bgImage)
            make.height.width.equalTo(80)
        }
        
        centerIcon.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.centerY.equalTo(bgImage)
            make.height.width.equalTo(50)

        }
        
        stackV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.top.equalTo(bgImage.snp.bottom).offset(20)
        }
        
        transcriptionView.snp.makeConstraints { (make) in
            make.height.equalTo(60)
        }
        
    }
    
    @objc func helpBtnClicked() {
        let VC = HelpAndSupportVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func languageClicked() {
        let VC = LanguagesVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @objc func audioBtnClicked() {
        let VC = VoiceRecorderVC()
        Application.Stored.isBackToAudio = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func callBtnClicked() {
        let VC = CallRecorderVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @objc func darkModeSwitched(_ sender: Any) {
        if C.Stored.darkModeEnabled == false {
            Application.setBoolValueToUserDefaults(true, forKey: C.Stored._darkModeEnabled)
            C.Stored.darkModeEnabled = true
            C.theme.theme = .dark
            darkModeV.switchBtn.status = true
            NotificationCenter.default.post(name: .darkModeEnabled, object: nil)
        } else {
            Application.setBoolValueToUserDefaults(false, forKey: C.Stored._darkModeEnabled)
            C.Stored.darkModeEnabled = false
            C.theme.theme = .light
            darkModeV.switchBtn.status = false
            NotificationCenter.default.post(name: .darkModeDisabled, object: nil)
        }
    }
    
    @objc private func darkModeEnabled(_ notification: Notification) {
        // Write your dark mode code here
        self.view.backgroundColor = C.theme.backgroundColor
        self.setttingsL.textColor = C.theme.textColor
        self.customNavigationBarView.backgroundColor = Application.Colors.mattLight
        darkModeV.switchBtn.status = true
    }
    
    @objc private func darkModeDisabled(_ notification: Notification) {
        self.view.backgroundColor = .white
        self.setttingsL.textColor = C.theme.textColor
        self.customNavigationBarView.backgroundColor = UIColor.white
        darkModeV.switchBtn.status = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkModeEnabled, object: nil)
        NotificationCenter.default.removeObserver(self, name: .darkModeDisabled, object: nil)
    }


}
