//
//  AccessFeatureVC.swift
//  Call Recording
//
//  Created by TechQuadra on 08/06/20.
//  Copyright © 2020 Vyclean Solutions. All rights reserved.
//

import UIKit

class AccessFeatureVC: UIViewController {
    
    var scrollV: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bouncesZoom = false
        scroll.backgroundColor = UIColor.clear
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()

    
    let topImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "start trial")
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        return image
    }()
    
    var firstPlanV: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = C.theme.navColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 25
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()
    
    var secondPlanV: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = C.theme.navColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.36
        view.layer.cornerRadius = 25
        view.layer.shadowColor = UIColor.darkGray.cgColor
        return view
    }()

    var FirstCheckBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "tick")
        btn.tintColor = UIColor.init(rgb: 0x057D08)
        btn.setImage(image, for: .normal)
        return btn
    }()

    var secondCheckBtn: UIButton = {
        let btn = UIButton()
        let image = #imageLiteral(resourceName: "untick").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        btn.tintColor = UIColor.init(rgb: 0x057D08)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    var firstPackL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.numberOfLines = 0
        l.font = UIFont(name: "Montserrat-Regular", size: 14)
        l.text = "3-Day Free Trial"
        return l
    }()

    var secondPackL: UILabel = {
        let l = UILabel()
        l.textColor = C.theme.textColor
        l.numberOfLines = 0
        l.font = UIFont(name: "Montserrat-Regular", size: 14)
        l.text = "3-Day Free Trial"
        return l
    }()

    var firstDescL: UILabel = {
        let l = UILabel()
        l.textAlignment = .right
        l.numberOfLines = 0
        l.font = UIFont(name: "", size: 13)
        l.textColor = C.theme.textColor
        return l
    }()
    
    var secondDescL: UILabel = {
        let l = UILabel()
        l.textAlignment = .right
        l.numberOfLines = 0
        l.font = UIFont(name: "", size: 13)
        l.textColor = C.theme.textColor
        return l
    }()


    let headingL: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        l.text = "Unlimited Access To All Features"
        l.textColor = C.theme.textColor
//        l.backgroundColor = UIColor.red
        return l
    }()
    
    var firstPackBtn: UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    var secondPackBtn: UIButton = {
        let btn = UIButton()
        return btn
    }()


    var continueBtn: UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(#imageLiteral(resourceName: "splash btn"), for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Start & Trial Plan", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 16)
        return btn
    }()
    
    var versionBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Or continue with limited version", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        btn.setTitleColor(C.theme.darkGrayTextColor, for: .normal)
        return btn
    }()
    
    var termsBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Terms of Use", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        btn.setTitleColor(C.theme.darkGrayTextColor, for: .normal)
        return btn
    }()

    var privacyBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Privacy Policy", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        btn.setTitleColor(C.theme.darkGrayTextColor, for: .normal)
        return btn
    }()
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 14),
        .foregroundColor: C.theme.textColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp()  {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = C.theme.backgroundColor
        firstDescL.attributedText = Application.attributedTextWith(text1: "Rs. 599.0 ", text2: "per week\nauto renewable", font1: UIFont(name: "Montserrat-Regular", size: 14) ?? UIFont(), font2: UIFont(name: "Montserrat-Regular", size: 10) ?? UIFont(), color1: C.theme.textColor, color2: C.theme.textColor)
        secondDescL.attributedText = Application.attributedTextWith(text1: "Rs. 599.0 ", text2: "per week\nauto renewable", font1: UIFont(name: "Montserrat-Regular", size: 14) ?? UIFont(), font2: UIFont(name: "Montserrat-Regular", size: 10) ?? UIFont(), color1: C.theme.textColor, color2: C.theme.textColor)
        firstPlanV.layer.borderWidth = 1
        firstPlanV.layer.borderColor = UIColor.init(rgb: 0x057D08).cgColor
        continueBtn.addTarget(self, action: #selector(continueClicked), for: .touchUpInside)
        versionBtn.addTarget(self, action: #selector(continueClicked), for: .touchUpInside)

        let attributeString = NSMutableAttributedString(string: "Or continue with limited version",attributes: yourAttributes)
        versionBtn.setAttributedTitle(attributeString, for: .normal)

        let attributeString1 = NSMutableAttributedString(string: "Terms of Use",attributes: yourAttributes)
        termsBtn.setAttributedTitle(attributeString1, for: .normal)

        let attributeString2 = NSMutableAttributedString(string: "Privacy Policy",attributes: yourAttributes)
        privacyBtn.setAttributedTitle(attributeString2, for: .normal)
        
        firstPackBtn.addTarget(self, action: #selector(firstPlanAction), for: .touchUpInside)
        secondPackBtn.addTarget(self, action: #selector(secondPlanAction), for: .touchUpInside)


    }
    
    func setUpViews()  {
        view.addSubview(scrollV)
        scrollV.addSubview(topImage)
        scrollV.addSubview(headingL)
        scrollV.addSubview(firstPlanV)
        scrollV.addSubview(secondPlanV)
        firstPlanV.addSubview(firstDescL)
        firstPlanV.addSubview(firstPackL)
        firstPlanV.addSubview(FirstCheckBtn)
        secondPlanV.addSubview(secondDescL)
        secondPlanV.addSubview(secondPackL)
        secondPlanV.addSubview(secondCheckBtn)
        firstPlanV.addSubview(firstPackBtn)
        secondPlanV.addSubview(secondPackBtn)
        scrollV.addSubview(continueBtn)
        scrollV.addSubview(versionBtn)
        scrollV.addSubview(termsBtn)
        scrollV.addSubview(privacyBtn)

        scrollV.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(view)
        }
        
        topImage.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(400)
            make.top.equalTo(scrollV)
        }
        
        headingL.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(topImage.snp.bottom).offset(10)
        }
        
        firstPlanV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.top.equalTo(headingL.snp.bottom).offset(15)
        }

        secondPlanV.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.top.equalTo(firstPlanV.snp.bottom).offset(15)
        }
        
        FirstCheckBtn.snp.makeConstraints { (make) in
            make.right.equalTo(firstPlanV).offset(-15)
            make.centerY.equalTo(firstPlanV)
            make.height.width.equalTo(25)
        }
        
        secondCheckBtn.snp.makeConstraints { (make) in
            make.right.equalTo(secondPlanV).offset(-15)
            make.centerY.equalTo(secondPlanV)
            make.height.width.equalTo(25)
        }
        
        firstPackL.snp.makeConstraints { (make) in
            make.left.equalTo(firstPlanV).offset(8)
            make.centerY.equalTo(firstPlanV)
        }
        
        secondPackL.snp.makeConstraints { (make) in
            make.left.equalTo(secondPlanV).offset(8)
            make.centerY.equalTo(secondPlanV)
        }
        
        firstDescL.snp.makeConstraints { (make) in
            make.right.equalTo(FirstCheckBtn.snp.left).offset(-10)
            make.centerY.equalTo(firstPlanV)
            make.width.equalTo(160)
        }

        secondDescL.snp.makeConstraints { (make) in
            make.right.equalTo(secondCheckBtn.snp.left).offset(-10)
            make.centerY.equalTo(secondPlanV)
            make.width.equalTo(160)
        }
        
        continueBtn.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(30)
            make.right.equalTo(view).offset(-30)
            make.top.equalTo(secondPlanV.snp.bottom).offset(20)
            make.height.equalTo(60)
        }
        
        versionBtn.snp.makeConstraints { (make) in
            make.top.equalTo(continueBtn.snp.bottom).offset(5)
            make.centerX.equalTo(view)
        }
        
        termsBtn.snp.makeConstraints { (make) in
            make.top.equalTo(versionBtn.snp.bottom).offset(15)
            make.centerX.equalTo(view)
            make.height.equalTo(20)
        }

        privacyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(termsBtn.snp.bottom).offset(5)
            make.centerX.equalTo(view)
            make.bottom.equalTo(scrollV.snp.bottom).offset(-20)
        }
        
        firstPackBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(firstPlanV)
        }
        
        secondPackBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(secondPlanV)
        }
    }

    @objc func continueClicked() {
        let VC = VoiceRecorderVC()
        UserDefaults.standard.set(true, forKey: Application.Stored.isLogin)
        Application.Stored.isBackToAudio = false
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func firstPlanAction() {
        self.secondPlanV.layer.borderColor = UIColor.clear.cgColor
        let image = #imageLiteral(resourceName: "untick").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        secondCheckBtn.tintColor = UIColor.init(rgb: 0x057D08)
        secondCheckBtn.setImage(image, for: .normal)
        FirstCheckBtn.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        firstPlanV.layer.borderWidth = 1
        firstPlanV.layer.borderColor = UIColor.init(rgb: 0x057D08).cgColor

    }
    
    @objc func secondPlanAction() {
        self.firstPlanV.layer.borderColor = UIColor.clear.cgColor
        let image = #imageLiteral(resourceName: "untick").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        FirstCheckBtn.tintColor = UIColor.init(rgb: 0x057D08)
        FirstCheckBtn.setImage(image, for: .normal)
        secondCheckBtn.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        secondPlanV.layer.borderWidth = 1
        secondPlanV.layer.borderColor = UIColor.init(rgb: 0x057D08).cgColor
    }

}
